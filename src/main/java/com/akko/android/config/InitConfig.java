package com.akko.android.config;


import com.akko.android.property.DUFileProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@Configuration
@EnableConfigurationProperties({
        DUFileProperties.class
})
public class InitConfig {
    @Bean
    WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                //registry.addMapping("/**").allowedOrigins("http://localhost:4200");
                registry.addMapping("/**");
            }
        };
    }

    @Bean
    void setTimezone() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}
