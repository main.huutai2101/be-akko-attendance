package com.akko.android.controller;

import com.akko.android.dto.SemesterDto;
import com.akko.android.entity.Semester;
import com.akko.android.entity.Status;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.SemesterRepository;
import com.akko.android.repository.StatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/semester")
public class SemesterController {
    @Autowired
    private SemesterRepository semesterRepository;
    @Autowired
    private StatusRepository statusRepository;


    @GetMapping
    public ResponseEntity<List<Semester>> list() {
        return ResponseEntity.ok(semesterRepository.findAll());
    }

    @GetMapping("{code}")
    public ResponseEntity<Semester> get(@PathVariable String code) {
        return Optional.ofNullable(semesterRepository.findFirstByCode(code))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found semester with code="+code); });
    }

    @PostMapping
    public ResponseEntity<Semester> create(@RequestBody final SemesterDto semesterDto) {
        //Check semester is existed or not by code
        Semester semester = semesterRepository.findFirstByCode(semesterDto.getCode());
        if (semester != null) throw new ConflictException("Semester with code="+semesterDto.getCode()+" is existed!");

        //Set data and save
        Semester newSemester = new ModelMapper().map(semesterDto, Semester.class);
        newSemester.setStatus(statusRepository.getOne(semesterDto.getStatusId()));
        return ResponseEntity.ok(semesterRepository.saveAndFlush(newSemester));
    }

    @DeleteMapping("{code}")
    public ResponseEntity<Void> delete(@PathVariable String code) {
        //Check semester is existed or not by code
        Semester semester = semesterRepository.findFirstByCode(code);
        if (semester == null) throw new NotFoundException("Not found semester with code="+code);
        semesterRepository.delete(semester);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{code}")
    public ResponseEntity<Semester> update(@PathVariable String code, @RequestBody SemesterDto semesterDto) {
        //Find semester
        Semester existingSemester = semesterRepository.findFirstByCode(code);
        if (existingSemester == null) throw new NotFoundException("Not found semester with code="+code);

        //Check conflict
        if (!code.equalsIgnoreCase(semesterDto.getCode())) {
            Semester checkSemester = semesterRepository.findFirstByCode(semesterDto.getCode());
            if (checkSemester != null) throw new ConflictException("Semester with code="+semesterDto.getCode()+" is existed!");
        }

        //Map with DTO
        Semester semester = new ModelMapper().map(semesterDto, Semester.class);
        Status s = statusRepository.getOne(semesterDto.getStatusId());
        semester.setStatus(s);

        //Copy it
        BeanUtils.copyProperties(semester, existingSemester, "id");
        return ResponseEntity.ok(semesterRepository.saveAndFlush(existingSemester));
    }
}
