package com.akko.android.controller;

import com.akko.android.dto.GGAuthDto;
import com.akko.android.entity.*;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.GGAuthRepository;
import com.akko.android.repository.RoleRepository;
import com.akko.android.repository.TeacherRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/gg-auth")
public class GGAuthController {
    @Autowired
    private GGAuthRepository ggAuthRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private TeacherRepository teacherRepository;

    @GetMapping
    public ResponseEntity<List<GGAuth>> list() {
        return ResponseEntity.ok(ggAuthRepository.findAll());
    }

    @GetMapping("{ggAuthId}")
    public ResponseEntity<GGAuth> get(@PathVariable String ggAuthId) {
        return Optional.ofNullable(ggAuthRepository.findFirstByGGAuthId(ggAuthId))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found Google Auth with ggId="+ggAuthId); });
    }

    @PostMapping
    public ResponseEntity<GGAuth> create(@RequestBody final GGAuthDto ggAuthDto) {
        //Check google auth existed or not by ggAuth
        GGAuth ggAuth = ggAuthRepository.findFirstByGGAuthId(ggAuthDto.getGGAuthId());
        if (ggAuth != null) throw new ConflictException("Google Auth with code="+ggAuthDto.getGGAuthId()+" is existed!");

        //Check information is have a google account
        GGAuth havGGAuth = teacherRepository.getOne(ggAuthDto.getTeacherId()).getGgAuth();
        if (havGGAuth != null) throw new ConflictException("Teacher with id="+ggAuthDto.getTeacherId()+" already has Google account!");

        //Set and save information for gg account
        GGAuth newGGAuth = new ModelMapper().map(ggAuthDto, GGAuth.class);
        newGGAuth.setRole(roleRepository.getOne(ggAuthDto.getRoleId()));
        newGGAuth.setTeacher(teacherRepository.getOne(ggAuthDto.getTeacherId()));

        return ResponseEntity.ok(ggAuthRepository.saveAndFlush(newGGAuth));
    }

    @DeleteMapping("{ggAuthId}")
    public ResponseEntity<Void> delete(@PathVariable String ggAuthId) {
        //Check google account is existed or not by code
        GGAuth ggAuth = ggAuthRepository.findFirstByGGAuthId(ggAuthId);
        if (ggAuth == null) throw new NotFoundException("Not found Google Auth with code="+ggAuthId);
        ggAuthRepository.delete(ggAuth);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{ggAuthId}")
    public ResponseEntity<GGAuth> update(@PathVariable String ggAuthId, @RequestBody GGAuthDto ggAuthDto) {
        //Find Google Auth
        GGAuth existingGgAuth = ggAuthRepository.findFirstByGGAuthId(ggAuthId);
        if (existingGgAuth == null) throw new NotFoundException("Not found Google Auth with code="+ggAuthId);

        //Check conflict for Google Auth code
        if (!ggAuthId.equalsIgnoreCase(ggAuthDto.getGGAuthId())) {
            GGAuth checkGgAuth = ggAuthRepository.findFirstByGGAuthId(ggAuthDto.getGGAuthId());
            if (checkGgAuth != null) throw new ConflictException("Google Auth with code="+ggAuthDto.getGGAuthId()+" is existed!");
        }

        //Check information is have a google account
        GGAuth haveGGAuth = teacherRepository.getOne(ggAuthDto.getTeacherId()).getGgAuth();
        if (haveGGAuth != null)
            if (!haveGGAuth.equals(existingGgAuth))
                throw new ConflictException("Teacher with id="+ggAuthDto.getTeacherId()+" already has Google account!");



        //Map with DTO
        GGAuth ggAuth = new ModelMapper().map(ggAuthDto, GGAuth.class);
        Role r = roleRepository.getOne(ggAuthDto.getRoleId());
        Teacher i = teacherRepository.getOne(ggAuthDto.getTeacherId());
        ggAuth.setRole(r);
        ggAuth.setTeacher(i);

        //Copy it
        BeanUtils.copyProperties(ggAuth, existingGgAuth, "id");
        return ResponseEntity.ok(ggAuthRepository.saveAndFlush(existingGgAuth));
    }
}
