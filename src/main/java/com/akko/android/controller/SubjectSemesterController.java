package com.akko.android.controller;

import com.akko.android.dto.SubjectSemesterDto;
import com.akko.android.entity.*;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/subject-semester")
public class SubjectSemesterController {
    @Autowired
    private SubjectSemesterRepository subjectSemesterRepository;
    @Autowired
    private SemesterRepository semesterRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private ClazzRepository clazzRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private SubjectSlotController subjectSlotController;

    @GetMapping
    public ResponseEntity<List<SubjectSemester>> list() {
        return ResponseEntity.ok(subjectSemesterRepository.findAll());
    }

    /**
     * Get by Id
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity<SubjectSemester> get(@PathVariable int id) {
        Optional<SubjectSemester> optional = subjectSemesterRepository.findById(id);
        if (optional.isEmpty()) throw new NotFoundException("Not found Subject Semester with id="+id);
        return ResponseEntity.ok(optional.get());
    }

    /**
     * Get list Slot with Subject Semester's Id
     * @param id
     * @return
     */
    @GetMapping("{id}/subject-slot")
    public ResponseEntity<List<SubjectSlot>> getSubjectSlot(@PathVariable int id) {
        Optional<SubjectSemester> optional = subjectSemesterRepository.findById(id);
        if (optional.isEmpty()) throw new NotFoundException("Not found Subject Semester with id="+id);
        return ResponseEntity.ok(optional.get().getSubjectSlots());
    }

    /**
     * Create new Subject Semester, auto create Slot and SubjectSession
     * @param subjectSemesterDto Subject in Semester
     * @return Subject Semester
     */
    @PostMapping
    public ResponseEntity<SubjectSemester> create(@RequestBody final SubjectSemesterDto subjectSemesterDto) {
        Status status = statusRepository.getOne(subjectSemesterDto.getStatusId());
        Semester semester = semesterRepository.getOne(subjectSemesterDto.getSemesterId());
        Subject subject = subjectRepository.getOne(subjectSemesterDto.getSubjectId());
        Clazz clazz = clazzRepository.getOne(subjectSemesterDto.getClazzId());
        Teacher teacher = teacherRepository.getOne(subjectSemesterDto.getTeacherId());

        //Check if this class has been scheduled for this subject
        SubjectSemester subjectSemester = subjectSemesterRepository.findFirstBySemesterAndSubjectAndClazz(semester, subject, clazz);
        if (subjectSemester != null) throw new ConflictException("The class " + clazz.getCode() + " has been scheduled for subject " + subject.getCode());

        //Set data
        SubjectSemester newSubjectSemester = new ModelMapper().map(subjectSemesterDto, SubjectSemester.class);
        newSubjectSemester.setStatus(status);
        newSubjectSemester.setSemester(semester);
        newSubjectSemester.setSubject(subject);
        newSubjectSemester.setClazz(clazz);
        newSubjectSemester.setTeacher(teacher);
        List<Student> studentList = new ArrayList<>();
        subjectSemesterDto.getStudentIdList().forEach(id -> studentList.add(studentRepository.getOne(id)));
        newSubjectSemester.setStudents(studentList);

        //Save data
        SubjectSemester savedSubjectSemester = subjectSemesterRepository.save(newSubjectSemester);

        //Create Subject Slot with new Thread
        new Thread(() -> subjectSlotController.generate(savedSubjectSemester, subjectSemesterDto)).start();
        return ResponseEntity.ok(savedSubjectSemester);
    }

    /**
     * Delete Subject Semester, detele the Slot and SubjectSession
     * @param id id of subject semester
     * @return void
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable int id) {
        //Check Subject Semester is existed or not by id
        Optional<SubjectSemester> subjectSemester = subjectSemesterRepository.findById(id);
        if (subjectSemester.isEmpty()) throw new NotFoundException("Not found Subject Semester with id="+id);
        SubjectSemester ss = subjectSemester.get();

        //Remove list Slot first
        subjectSlotController.remove(ss);
        subjectSemesterRepository.delete(ss);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
