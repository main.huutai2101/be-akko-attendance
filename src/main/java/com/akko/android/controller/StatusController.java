package com.akko.android.controller;

import com.akko.android.dto.SemesterDto;
import com.akko.android.entity.Major;
import com.akko.android.entity.Semester;
import com.akko.android.entity.Status;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.SemesterRepository;
import com.akko.android.repository.StatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/status")
public class StatusController {
    @Autowired
    private StatusRepository statusRepository;


    @GetMapping
    public ResponseEntity<List<Status>> list() {
        return ResponseEntity.ok(statusRepository.findAll());
    }

    @GetMapping("{code}")
    public ResponseEntity<Status> get(@PathVariable String code) {
        return Optional.of(statusRepository.findFirstByCode(code))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found status with code="+code); });
    }

    @PostMapping
    public ResponseEntity<Status> create(@RequestBody final Status status) {
        //Check status is existed or not by code
        Status checkStatus = statusRepository.findFirstByCode(status.getCode());
        if (checkStatus != null) throw new ConflictException("Status with code="+status.getCode()+" is existed!");

        //Set data and save
        return ResponseEntity.ok(statusRepository.saveAndFlush(status));
    }

    @DeleteMapping("{code}")
    public ResponseEntity<Void> delete(@PathVariable String code) {
        //Check status is existed or not by code
        Status status = statusRepository.findFirstByCode(code);
        if (status == null) throw new NotFoundException("Not found status with code="+code);
        statusRepository.deleteById(status.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{code}")
    public ResponseEntity<Status> update(@PathVariable String code, @RequestBody Status status) {
        //Find status
        Status existingStatus = statusRepository.findFirstByCode(code);
        if (existingStatus == null) throw new NotFoundException("Not found status with code="+code);

        //Check conflict
        if (!code.equalsIgnoreCase(status.getCode())) {
            Status checkStatus = statusRepository.findFirstByCode(status.getCode());
            if (checkStatus != null) throw new ConflictException("Status with code="+status.getCode()+" is existed!");
        }

        //Copy it
        BeanUtils.copyProperties(status, existingStatus, "id");
        return ResponseEntity.ok(statusRepository.saveAndFlush(existingStatus));
    }
}
