package com.akko.android.controller;

import com.akko.android.dto.MajorDto;
import com.akko.android.entity.Major;
import com.akko.android.entity.Status;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.MajorRepository;
import com.akko.android.repository.StatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/major")
public class MajorController {
    @Autowired
    private MajorRepository majorRepository;
    @Autowired
    private StatusRepository statusRepository;


    @GetMapping
    public ResponseEntity<List<Major>> list() {
        return ResponseEntity.ok(majorRepository.findAll());
    }

    @GetMapping("{code}")
    public ResponseEntity<Major> get(@PathVariable String code) {
        return Optional.ofNullable(majorRepository.findFirstByCode(code))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found major with code="+code); });
    }

    @PostMapping
    public ResponseEntity<Major> create(@RequestBody final MajorDto majorDto) {
        //Check major is existed or not by code
        Major major = majorRepository.findFirstByCode(majorDto.getCode());
        if (major != null) throw new ConflictException("Major with code="+majorDto.getCode()+" is existed!");

        //Set data and save
        Major newMajor = new ModelMapper().map(majorDto, Major.class);
        newMajor.setStatus(statusRepository.getOne(majorDto.getStatusId()));
        return ResponseEntity.ok(majorRepository.saveAndFlush(newMajor));
    }

    @DeleteMapping("{code}")
    public ResponseEntity<Void> delete(@PathVariable String code) {
        //Check major is existed or not by code
        Major major = majorRepository.findFirstByCode(code);
        if (major == null) throw new NotFoundException("Not found major with code="+code);
        majorRepository.delete(major);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{code}")
    public ResponseEntity<Major> update(@PathVariable String code, @RequestBody MajorDto majorDto) {
        //Find major
        Major existingClazz = majorRepository.findFirstByCode(code);
        if (existingClazz == null) throw new NotFoundException("Not found major with code="+code);

        //Check conflict
        if (!code.equalsIgnoreCase(majorDto.getCode())) {
            Major checkMajor = majorRepository.findFirstByCode(majorDto.getCode());
            if (checkMajor != null) throw new ConflictException("Major with code="+majorDto.getCode()+" is existed!");
        }

        //Map with DTO
        Major major = new ModelMapper().map(majorDto, Major.class);
        Status s = statusRepository.getOne(majorDto.getStatusId());
        major.setStatus(s);

        //Copy it
        BeanUtils.copyProperties(major, existingClazz, "id");
        return ResponseEntity.ok(majorRepository.saveAndFlush(existingClazz));
    }
}
