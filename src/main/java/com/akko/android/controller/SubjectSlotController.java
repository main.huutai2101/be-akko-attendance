package com.akko.android.controller;

import com.akko.android.dto.SubjectSemesterDto;
import com.akko.android.entity.*;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.RoomRepository;
import com.akko.android.repository.StatusRepository;
import com.akko.android.repository.SubjectSlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@RestController
@RequestMapping("/subject-slot")
public class SubjectSlotController {
    @Autowired
    private SubjectSlotRepository subjectSlotRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private StatusRepository statusRepository;


    @Autowired
    private SubjectSessionController subjectSessionController;

    //Slot with time:              1   2   3    4   5   6   7   8
    private final int[] hours =   {0,  1,  3,   5,  7,  9, 11, 13};
    private final int[] minutes = {0, 45,  30, 45, 30, 15, 30, 15};

    /**
     * Generate every create new class
     * @param subjectSemester Subject in Semester
     * @param subjectSemesterDto Subject in Semester
     */
    public void generate(SubjectSemester subjectSemester, SubjectSemesterDto subjectSemesterDto) {
        //Get some data
        List<Integer> scheduled = subjectSemesterDto.getScheduled();
        Room room = roomRepository.getOne(subjectSemesterDto.getRoomId());
        Status status = statusRepository.findFirstByCode("NOT_YET");

        LocalDate today = LocalDate.now();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(subjectSemester.getStartDate());

        int n = subjectSemester.getNumberOfSlots();
        int d = - 1;
        int slot;

        for(int i=0; i<n; ++i) {
            do {
                d = ++d % 56;
                slot = d % 8;
                if (scheduled.contains(d)) {
                    Date time = calendar.getTime();
                    time.setHours(hours[slot]);
                    time.setMinutes(minutes[slot]);
                    time.setSeconds(0);

                    SubjectSlot subjectSlot = new SubjectSlot(i+1, time);
                    subjectSlot.setSubjectSemester(subjectSemester);
                    subjectSlot.setRoom(room);
                    subjectSlot.setStatus(status);

                    LocalDate localDate = time.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    if (localDate.isBefore(today)) subjectSlot.setStatus(statusRepository.findFirstByCode("CLOSE"));


                    SubjectSlot savedSubjectSlot = subjectSlotRepository.save(subjectSlot);

                    //Generate Subject Session for Student
                    subjectSessionController.generate(savedSubjectSlot, subjectSemester.getStudents());

                    break;
                }
                if (slot == 7) calendar.add(Calendar.DATE,  1);
            } while (true);
        }
    }


    /**
     * Get details one slot by Id
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity<SubjectSlot> getById(@PathVariable int id) {
        Optional<SubjectSlot> optional = subjectSlotRepository.findById(id);
        if (optional.isEmpty()) throw new NotFoundException("Not found Slot with id="+id);
        return ResponseEntity.ok(optional.get());
    }

    /**
     * Remove list Slot with SubjectSemester
     * @param subjectSemester
     */
    public void remove(SubjectSemester subjectSemester) {
        subjectSemester.getSubjectSlots().forEach(slot -> {
            subjectSessionController.remove(slot);
        });
        subjectSlotRepository.deleteAll(subjectSemester.getSubjectSlots());
    }
}
