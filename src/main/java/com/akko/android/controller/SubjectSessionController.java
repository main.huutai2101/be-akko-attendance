package com.akko.android.controller;

import com.akko.android.entity.*;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.StatusRepository;
import com.akko.android.repository.SubjectSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/subject-session")
public class SubjectSessionController {

    @Autowired
    private SubjectSessionRepository subjectSessionRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private TeacherController teacherController;

    @GetMapping("/teacher/{code}")
    public ResponseEntity<List<SubjectSession>> getSessionByTeacherCode(@PathVariable String code) {
        ResponseEntity<List<SubjectSemester>> resSubjectSemester = teacherController.getSubjectSemestersByTeacherCode(code);
        if (resSubjectSemester.getStatusCodeValue() != 200) throw new NotFoundException("Cannot get Session by Teacher code="+code);
        List<SubjectSemester> subjectSemesters = resSubjectSemester.getBody();
        assert subjectSemesters != null;
        List<SubjectSession> sessions = new ArrayList<>();
        for (SubjectSemester ss : subjectSemesters) {
            for (SubjectSlot slot : ss.getSubjectSlots()) {
                sessions.addAll(subjectSessionRepository.findAllBySlot(slot));
            }
        }
        return ResponseEntity.ok(sessions);
    }


    /**
     * Method to generate subject session when create the scheduled
     * @param subjectSlot
     * @param studentList
     */
    public void generate(SubjectSlot subjectSlot, List<Student> studentList) {
        AtomicReference<Status> status = new AtomicReference<>(statusRepository.findFirstByCode("NOT_YET"));

        studentList.forEach(student -> {
            SubjectSession ss = new SubjectSession();

            if (!subjectSlot.getStatus().getCode().equals("NOT_YET")) {
                Status[] s = {statusRepository.findFirstByCode("ATTENDED"),statusRepository.findFirstByCode("ABSENT")};
                status.set(s[new Random().nextInt(s.length)]);
            }

            ss.setSlot(subjectSlot);
            ss.setStatus(status.get());
            ss.setStudent(student);
            subjectSessionRepository.saveAndFlush(ss);
        });
    }

    /**
     * Remove SubjectSession by Slot's Id
     * @param slot
     */
    public void remove(SubjectSlot slot) {
        subjectSessionRepository.deleteAll(slot.getSubjectSessions());
    }
}
