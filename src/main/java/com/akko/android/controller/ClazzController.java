package com.akko.android.controller;

import com.akko.android.dto.ClazzDto;
import com.akko.android.entity.Clazz;
import com.akko.android.entity.Status;
import com.akko.android.entity.SubjectSemester;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.ClazzRepository;
import com.akko.android.repository.StatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/class")
public class ClazzController {
    @Autowired
    private ClazzRepository clazzRepository;
    @Autowired
    private StatusRepository statusRepository;

    @GetMapping
    public ResponseEntity<List<Clazz>> list() {
        return ResponseEntity.ok(clazzRepository.findAll());
    }

    @GetMapping("{code}")
    public ResponseEntity<Clazz> get(@PathVariable String code) {
        return Optional.ofNullable(clazzRepository.findFirstByCode(code))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found class with code="+code); });
    }

    @PostMapping
    public ResponseEntity<Clazz> create(@RequestBody final ClazzDto clazzDto) {
        //Check class is existed or not by code
        Clazz clazz = clazzRepository.findFirstByCode(clazzDto.getCode());
        if (clazz != null) throw new ConflictException("Class with code="+clazzDto.getCode()+" is existed!");

        //Set data and save
        Clazz newClazz = new ModelMapper().map(clazzDto, Clazz.class);
        newClazz.setStatus(statusRepository.getOne(clazzDto.getStatusId()));
        return ResponseEntity.ok(clazzRepository.saveAndFlush(newClazz));
    }

    @DeleteMapping("{code}")
    public ResponseEntity<Void> delete(@PathVariable String code) {
        //Check class is existed or not by code
        Clazz clazz = clazzRepository.findFirstByCode(code);
        if (clazz == null) throw new NotFoundException("Not found class with code="+code);
        clazzRepository.delete(clazz);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{code}")
    public ResponseEntity<Clazz> update(@PathVariable String code, @RequestBody ClazzDto clazzDto) {
        //Find class
        Clazz existingClazz = clazzRepository.findFirstByCode(code);
        if (existingClazz == null) throw new NotFoundException("Not found class with code="+code);

        //Check conflict
        if (!code.equalsIgnoreCase(clazzDto.getCode())) {
            Clazz checkClazz = clazzRepository.findFirstByCode(clazzDto.getCode());
            if (checkClazz != null) throw new ConflictException("Class with code="+clazzDto.getCode()+" is existed!");
        }

        //Map with DTO
        Clazz clazz = new ModelMapper().map(clazzDto, Clazz.class);
        Status s = statusRepository.getOne(clazzDto.getStatusId());
        clazz.setStatus(s);

        //Copy it
        BeanUtils.copyProperties(clazz, existingClazz, "id");
        return ResponseEntity.ok(clazzRepository.saveAndFlush(existingClazz));
    }

    @Autowired
    private TeacherController teacherController;

    @GetMapping("teacher/{code}")
    public ResponseEntity<List<Clazz>> getAllByTeacherCode(@PathVariable String code) {
        ResponseEntity<List<SubjectSemester>> resListSubjectSemester = teacherController.getSubjectSemestersByTeacherCode(code);
        if (resListSubjectSemester.getStatusCodeValue() != 200) throw new NotFoundException("Cannot find Clazz by Teacher code");

        List<Clazz> clazzList = new ArrayList<>();
        Objects.requireNonNull(resListSubjectSemester.getBody()).forEach(ss -> clazzList.add(ss.getClazz()));
        return ResponseEntity.ok(clazzList);
    }
}
