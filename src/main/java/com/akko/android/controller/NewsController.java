package com.akko.android.controller;

import com.akko.android.entity.News;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.NewsRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/news")
public class NewsController {
    @Autowired
    private NewsRepository newsRepository;

    @GetMapping
    public ResponseEntity<List<News>> list() {
        return ResponseEntity.ok(newsRepository.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<News> get(@PathVariable int id) {
        return Optional.of(newsRepository.getOne(id))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found News with id="+id); });
    }

    @PostMapping
    public ResponseEntity<News> create(@RequestBody final News news) {
        //Set data and save
        return ResponseEntity.ok(newsRepository.saveAndFlush(news));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable int id) {
        //Check news is existed or not by code
        Optional<News> optionalNews = newsRepository.findById(id);
        if (optionalNews.isEmpty()) throw new NotFoundException("Not found news with id="+id);
        newsRepository.delete(optionalNews.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<News> update(@PathVariable int id, @RequestBody News news) {
        //Find news
        Optional<News> existingStatus = newsRepository.findById(id);
        if (existingStatus.isEmpty()) throw new NotFoundException("Not found news with id="+id);

        //Copy it
        News newNews = existingStatus.get();
        BeanUtils.copyProperties(news, newNews, "id");
        return ResponseEntity.ok(newsRepository.saveAndFlush(newNews));
    }

    @GetMapping("/update/{id}")
    public ResponseEntity<List<News>> updateNews(@PathVariable int id) {
        return ResponseEntity.ok(newsRepository.findAllByIdGreaterThan(id));
    }

    @GetMapping("/latest/{n}")
    public ResponseEntity<List<News>> latest(@PathVariable int n) {
        List<News> list = newsRepository.findAll();
        list = list.subList(Math.max(list.size() - n, 0), list.size());
        return ResponseEntity.ok(list);
    }


    @GetMapping("/scroll")
    public ResponseEntity<List<News>> scroll(@RequestParam int id, @RequestParam int size) {
        List<News> list = newsRepository.findAllByIdLessThan(id);
        Collections.reverse(list);
        list = list.subList(0, Math.min(size, list.size()));
        return ResponseEntity.ok(list);
    }

    @GetMapping("/search")
    public ResponseEntity<List<News>> scroll(@RequestParam String key) {
        key = '%' + key + '%';
        List<News> list = newsRepository.findAllByTitleLikeOrContentLike(key, key);
        return ResponseEntity.ok(list);
    }


}
