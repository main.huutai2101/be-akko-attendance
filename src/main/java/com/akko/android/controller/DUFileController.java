package com.akko.android.controller;

import com.akko.android.property.DUFileProperties;
import com.akko.android.service.DUFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class DUFileController {

    private static final Logger logger = LoggerFactory.getLogger(DUFileController.class);

    @Autowired
    private DUFileService DUFileService;
    @Autowired
    private DUFileProperties duFileProperties;

    @GetMapping(value = "${file.download-uri}/**")
    public ResponseEntity<Resource> downloadFile(HttpServletRequest request) {
        //Get path of file
        String uri = request.getRequestURI().replace("-","/");
        String path = uri.substring(duFileProperties.getDownloadUri().length()+1);

        // Load file as Resource
        Resource resource = DUFileService.loadFileAsResource(path);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
