package com.akko.android.controller;

import com.akko.android.dto.SubjectDto;
import com.akko.android.entity.Subject;
import com.akko.android.entity.Subject;
import com.akko.android.entity.Status;
import com.akko.android.entity.SubjectSemester;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.SubjectRepository;
import com.akko.android.repository.StatusRepository;
import com.akko.android.repository.SubjectRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/subject")
public class SubjectController {
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private StatusRepository statusRepository;


    @GetMapping
    public ResponseEntity<List<Subject>> list() {
        return ResponseEntity.ok(subjectRepository.findAll());
    }

    @GetMapping("{code}")
    public ResponseEntity<Subject> get(@PathVariable String code) {
        return Optional.ofNullable(subjectRepository.findFirstByCode(code))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found subject with code="+code); });
    }

    @PostMapping
    public ResponseEntity<Subject> create(@RequestBody final SubjectDto subjectDto) {
        //Check subject is existed or not by code
        Subject subject = subjectRepository.findFirstByCode(subjectDto.getCode());
        if (subject != null) throw new ConflictException("Subject with code="+subjectDto.getCode()+" is existed!");

        //Set data and save
        Subject newSubject = new ModelMapper().map(subjectDto, Subject.class);
        newSubject.setStatus(statusRepository.getOne(subjectDto.getStatusId()));
        return ResponseEntity.ok(subjectRepository.saveAndFlush(newSubject));
    }

    @DeleteMapping("{code}")
    public ResponseEntity<Void> delete(@PathVariable String code) {
        //Check subject is existed or not by code
        Subject subject = subjectRepository.findFirstByCode(code);
        if (subject == null) throw new NotFoundException("Not found subject with code="+code);
        subjectRepository.delete(subject);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{code}")
    public ResponseEntity<Subject> update(@PathVariable String code, @RequestBody SubjectDto subjectDto) {
        //Find subject
        Subject existingSubject = subjectRepository.findFirstByCode(code);
        if (existingSubject == null) throw new NotFoundException("Not found subject with code="+code);

        //Check conflict
        if (!code.equalsIgnoreCase(subjectDto.getCode())) {
            Subject checkSubject = subjectRepository.findFirstByCode(subjectDto.getCode());
            if (checkSubject != null) throw new ConflictException("Subject with code="+subjectDto.getCode()+" is existed!");
        }

        //Map with DTO
        Subject subject = new ModelMapper().map(subjectDto, Subject.class);
        Status s = statusRepository.getOne(subjectDto.getStatusId());
        subject.setStatus(s);

        //Copy it
        BeanUtils.copyProperties(subject, existingSubject, "id");
        return ResponseEntity.ok(subjectRepository.saveAndFlush(existingSubject));
    }

    @Autowired
    private TeacherController teacherController;
    @GetMapping("teacher/{code}")
    public ResponseEntity<List<Subject>> getAllByTeacherCode(@PathVariable String code) {
        ResponseEntity<List<SubjectSemester>> resListSubjectSemester = teacherController.getSubjectSemestersByTeacherCode(code);
        if (resListSubjectSemester.getStatusCodeValue() != 200) throw new NotFoundException("Cannot find Subject by Teacher code");

        List<Subject> subjectList = new ArrayList<>();
        Objects.requireNonNull(resListSubjectSemester.getBody()).forEach(ss -> subjectList.add(ss.getSubject()));
        return ResponseEntity.ok(subjectList);
    }
}
