package com.akko.android.controller;

import com.akko.android.dto.RoomDto;
import com.akko.android.entity.Room;
import com.akko.android.entity.Status;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.RoomRepository;
import com.akko.android.repository.StatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/room")
public class RoomController {
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private StatusRepository statusRepository;


    @GetMapping
    public ResponseEntity<List<Room>> list() {
        return ResponseEntity.ok(roomRepository.findAll());
    }

    @GetMapping("{code}")
    public ResponseEntity<Room> get(@PathVariable String code) {
        return Optional.ofNullable(roomRepository.findFirstByCode(code))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found room with code="+code); });
    }

    @PostMapping
    public ResponseEntity<Room> create(@RequestBody final RoomDto roomDto) {
        //Check room is existed or not by code
        Room room = roomRepository.findFirstByCode(roomDto.getCode());
        if (room != null) throw new ConflictException("Room with code="+roomDto.getCode()+" is existed!");

        //Set data and save
        Room newClazz = new ModelMapper().map(roomDto, Room.class);
        newClazz.setStatus(statusRepository.getOne(roomDto.getStatusId()));
        return ResponseEntity.ok(roomRepository.saveAndFlush(newClazz));
    }

    @DeleteMapping("{code}")
    public ResponseEntity<Void> delete(@PathVariable String code) {
        //Check room is existed or not by code
        Room room = roomRepository.findFirstByCode(code);
        if (room == null) throw new NotFoundException("Not found room with code="+code);
        roomRepository.delete(room);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{code}")
    public ResponseEntity<Room> update(@PathVariable String code, @RequestBody RoomDto roomDto) {
        //Find room edit
        Room existingRoom = roomRepository.findFirstByCode(code);
        if (existingRoom == null) throw new NotFoundException("Not found room with code="+code);

        //Check conflict
        if (!code.equalsIgnoreCase(roomDto.getCode())) {
            Room checkRoom = roomRepository.findFirstByCode(roomDto.getCode());
            if (checkRoom != null) throw new ConflictException("Room with code="+roomDto.getCode()+" is existed!");
        }

        //Map with DTO
        Room room = new ModelMapper().map(roomDto, Room.class);
        Status s = statusRepository.getOne(roomDto.getStatusId());
        room.setStatus(s);

        //Copy it
        BeanUtils.copyProperties(room, existingRoom, "id");
        return ResponseEntity.ok(roomRepository.saveAndFlush(existingRoom));
    }
}
