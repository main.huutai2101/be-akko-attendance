package com.akko.android.controller;

import com.akko.android.dto.RoleDto;
import com.akko.android.entity.Role;
import com.akko.android.entity.Role;
import com.akko.android.entity.Status;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.RoleRepository;
import com.akko.android.repository.StatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private StatusRepository statusRepository;


    @GetMapping
    public ResponseEntity<List<Role>> list() {
        return ResponseEntity.ok(roleRepository.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<Role> get(@PathVariable int id) {
        return Optional.of(roleRepository.getOne(id))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found role with id="+id); });
    }

    @PostMapping
    public ResponseEntity<Role> create(@RequestBody final RoleDto roleDto) {
        //Set data and save
        Role newRole = new ModelMapper().map(roleDto, Role.class);
        newRole.setStatus(statusRepository.getOne(roleDto.getStatusId()));
        return ResponseEntity.ok(roleRepository.saveAndFlush(newRole));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable int id) {
        //Check class is existed or not by id
        Optional<Role> role = roleRepository.findById(id);
        if (role.isEmpty()) throw new NotFoundException("Not found role with id="+id);
        roleRepository.delete(role.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<Role> update(@PathVariable int id, @RequestBody RoleDto roleDto) {
        //Find role
        Optional<Role> existingRole = roleRepository.findById(id);
        if (existingRole.isEmpty()) throw new NotFoundException("Not found role with id="+id);
        Role oldRole = existingRole.get();
        //Map with DTO
        Role role = new ModelMapper().map(roleDto, Role.class);
        Status s = statusRepository.getOne(roleDto.getStatusId());
        role.setStatus(s);

        //Copy it
        BeanUtils.copyProperties(role, oldRole, "id");
        return ResponseEntity.ok(roleRepository.saveAndFlush(oldRole));
    }
}
