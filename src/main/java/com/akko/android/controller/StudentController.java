package com.akko.android.controller;

import com.akko.android.dto.SemesterDto;
import com.akko.android.dto.StudentDto;
import com.akko.android.entity.*;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.*;
import com.akko.android.service.DUFileService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private MajorRepository majorRepository;
    @Autowired
    private ClazzRepository clazzRepository;
    @Autowired
    private DUFileService duFileService;


    @GetMapping
    public ResponseEntity<List<Student>> list() {
        return ResponseEntity.ok(studentRepository.findAll());
    }

    @GetMapping("class/{idClass}")
    public ResponseEntity<List<Student>> getByClass(@PathVariable int idClass) {
        Optional<Clazz> optionalClazz = clazzRepository.findById(idClass);
        if (optionalClazz.isEmpty()) throw new NotFoundException("Not found list student class id="+idClass);
        return ResponseEntity.ok(studentRepository.findAllByClazz(optionalClazz.get()));
    }

    @GetMapping("{code}")
    public ResponseEntity<Student> get(@PathVariable String code) {
        return Optional.ofNullable(studentRepository.findFirstByCode(code))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found student with code="+code); });
    }

    @PostMapping
    public ResponseEntity<Student> create(@ModelAttribute final StudentDto studentDto) {
        //Check student is existed or not by code
        Student student = studentRepository.findFirstByCode(studentDto.getCode());
        if (student != null) throw new ConflictException("Student with code="+student.getCode()+" is existed!");

        Student newStudent = mapDto(studentDto);

        return ResponseEntity.ok(studentRepository.saveAndFlush(newStudent));
    }

    @DeleteMapping("{code}")
    public ResponseEntity<Void> delete(@PathVariable String code) {
        //Check student is existed or not by code
        Student student = studentRepository.findFirstByCode(code);
        if (student == null) throw new NotFoundException("Not found student with code="+code);
        studentRepository.delete(student);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{code}")
    public ResponseEntity<Student> update(@PathVariable String code, @ModelAttribute StudentDto studentDto) {
        //Find student
        Student existingStudent = studentRepository.findFirstByCode(code);
        if (existingStudent == null) throw new NotFoundException("Not found student with code="+code);

        //Check conflict
        if (!code.equalsIgnoreCase(studentDto.getCode())) {
            Student checkStudent = studentRepository.findFirstByCode(studentDto.getCode());
            if (checkStudent != null) throw new ConflictException("Student with code="+studentDto.getCode()+" is existed!");
        }

        Student newStudent = mapDto(studentDto);

        //Copy data from student to existingStudent
        BeanUtils.copyProperties(newStudent, existingStudent, "id");
        return ResponseEntity.ok(studentRepository.saveAndFlush(existingStudent));
    }

    private Student mapDto(StudentDto studentDto) {
        //Get relationship
        Status status =  statusRepository.getOne(studentDto.getStatusId());
        Major major = majorRepository.getOne(studentDto.getMajorId());
        Clazz clazz = clazzRepository.getOne(studentDto.getClazzId());

        //Get path and storage image
        DUFile image = null;
        if (studentDto.getImage() != null) {
            String path = "Students/" + clazz.getCode();
            image = duFileService.storeFile(studentDto.getImage(), path, studentDto.getCode());
        }

        //Set data and save
        Student newStudent = new ModelMapper().map(studentDto, Student.class);
        newStudent.setStatus(status);
        newStudent.setMajor(major);
        newStudent.setClazz(clazz);
        if (image != null) newStudent.setImage(image.getFileDownloadUri());
        else newStudent.setImage(null);
        return newStudent;
    }
}
