package com.akko.android.controller;

import com.akko.android.dto.TeacherDto;
import com.akko.android.entity.DUFile;
import com.akko.android.entity.SubjectSemester;
import com.akko.android.entity.SubjectSlot;
import com.akko.android.entity.Teacher;
import com.akko.android.exception.ConflictException;
import com.akko.android.exception.NotFoundException;
import com.akko.android.repository.*;
import com.akko.android.service.DUFileService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@RestController
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private SubjectSlotRepository subjectSlotRepository;
    @Autowired
    private DUFileService duFileService;


    @GetMapping
    public ResponseEntity<List<Teacher>> list() {
        return ResponseEntity.ok(teacherRepository.findAll());
    }

    @GetMapping("{code}")
    public ResponseEntity<Teacher> get(@PathVariable String code) {
        return Optional.ofNullable(teacherRepository.findFirstByCode(code))
                .map(ResponseEntity::ok)
                .orElseGet(() -> { throw new NotFoundException("Not found teacher with code="+code); });
    }

    @GetMapping("{code}/subject-semesters")
    public ResponseEntity<List<SubjectSemester>> getSubjectSemestersByTeacherCode(@PathVariable String code) {
        return Optional.ofNullable(teacherRepository.findFirstByCode(code))
                .map(teacher -> ResponseEntity.ok(teacher.getSubjectSemesters()))
                .orElseGet(() -> { throw new NotFoundException("Not found teacher with code="+code); });
    }

    /**
     * Get slots today that teacher teach
     * @param code teacher code
     * @return list slots
     */
    @GetMapping("{code}/subject-slots")
    public ResponseEntity<List<SubjectSlot>> getSlotTodayByTeacherCode(@PathVariable String code) {
        return Optional.ofNullable(teacherRepository.findFirstByCode(code))
                .map(teacher -> {
                    List<SubjectSlot> slots = new ArrayList<>();

                    //Get startDate
                    Date startDate = new Date();
                    startDate.setHours(0);
                    startDate.setMinutes(0);

                    //Get endDate
                    Date endDate = new Date();
                    endDate.setHours(23);
                    endDate.setMinutes(59);

                    List<SubjectSemester> subjectSemesters = this.getSubjectSemestersByTeacherCode(code).getBody();
                    assert subjectSemesters != null;
                    subjectSemesters.forEach(subjectSemester -> {
                        List<SubjectSlot> slotList = subjectSlotRepository.findAllByDateGreaterThanEqualAndDateLessThanEqualAndSubjectSemester(startDate, endDate, subjectSemester);
                        slots.addAll(slotList);
                    });

                    return ResponseEntity.ok(slots);
                })
                .orElseGet(() -> { throw new NotFoundException("Not found teacher with code="+code); });
    }

    @GetMapping("{code}/all-slots")
    public ResponseEntity<List<SubjectSlot>> getAllSlots(@PathVariable String code) {
        return Optional.ofNullable(teacherRepository.findFirstByCode(code))
                .map(teacher -> {
                    List<SubjectSlot> slots = new ArrayList<>();


                    List<SubjectSemester> subjectSemesters = this.getSubjectSemestersByTeacherCode(code).getBody();
                    assert subjectSemesters != null;
                    subjectSemesters.forEach(subjectSemester -> {
                        List<SubjectSlot> slotList = subjectSlotRepository.findAllBySubjectSemester(subjectSemester);
                        slots.addAll(slotList);
                    });

                    return ResponseEntity.ok(slots);
                })
                .orElseGet(() -> { throw new NotFoundException("Not found teacher with code="+code); });
    }

    @PostMapping
    public ResponseEntity<Teacher> create(@ModelAttribute final TeacherDto teacherDto) {
        //Check teacher is existed or not by code
        Teacher teacher = teacherRepository.findFirstByCode(teacherDto.getCode());
        if (teacher != null) throw new ConflictException("Teacher with code="+teacher.getCode()+" is existed!");

        Teacher newTeacher = mapDto(teacherDto, new Teacher());
        
        return ResponseEntity.ok(teacherRepository.saveAndFlush(newTeacher));
    }

    @DeleteMapping("{code}")
    public ResponseEntity<Void> delete(@PathVariable String code) {
        //Check teacher is existed or not by code
        Teacher teacher = teacherRepository.findFirstByCode(code);
        if (teacher == null) throw new NotFoundException("Not found teacher with code="+code);
        teacherRepository.delete(teacher);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{code}")
    public ResponseEntity<Teacher> update(@PathVariable String code, @ModelAttribute TeacherDto teacherDto) {
        //Find teacher
        Teacher existingTeacher = teacherRepository.findFirstByCode(code);
        if (existingTeacher == null) throw new NotFoundException("Not found teacher with code="+code);

        //Check conflict
        if (!code.equalsIgnoreCase(teacherDto.getCode())) {
            Teacher checkTeacher = teacherRepository.findFirstByCode(teacherDto.getCode());
            if (checkTeacher != null) throw new ConflictException("Teacher with code="+teacherDto.getCode()+" is existed!");
        }

        Teacher newTeacher = mapDto(teacherDto, existingTeacher);

        //Copy data from teacher to existingTeacher
        if (newTeacher.getImage() == null) newTeacher.setImage(existingTeacher.getImage());
        BeanUtils.copyProperties(newTeacher, existingTeacher, "id");
        return ResponseEntity.ok(teacherRepository.saveAndFlush(existingTeacher));
    }

    private Teacher mapDto(TeacherDto teacherDto, Teacher teacher) {
        DUFile image = null;
        if (teacherDto.getImage() != null) {
            image = duFileService.storeFile(teacherDto.getImage(), "Teachers", teacherDto.getCode());
        }

        DUFile coverImage = null;
        if (teacherDto.getCoverImage() != null) {
            coverImage = duFileService.storeFile(teacherDto.getCoverImage(), "Covers", teacherDto.getCode());
        }

        //Map with DTO
        Teacher newTeacher = new ModelMapper().map(teacherDto, Teacher.class);
        newTeacher.setStatus(statusRepository.getOne(teacherDto.getStatusId()));

        if (image != null) newTeacher.setImage(image.getFileDownloadUri());
        else if (teacher.getImage() == null) newTeacher.setImage("http://localhost:8000/download/Teachers/default.png");
        else newTeacher.setImage(teacher.getImage());

        if (coverImage != null) newTeacher.setCoverImage(coverImage.getFileDownloadUri());
        else if (teacher.getImage() == null) newTeacher.setCoverImage("http://localhost:8000/download/Covers/default.jpg");
        else newTeacher.setCoverImage(teacher.getCoverImage());

        return  newTeacher;
    }
}
