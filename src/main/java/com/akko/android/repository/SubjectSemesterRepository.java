package com.akko.android.repository;

import com.akko.android.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubjectSemesterRepository extends JpaRepository<SubjectSemester, Integer> {
    SubjectSemester findFirstBySemesterAndSubjectAndClazz(Semester semester, Subject subject, Clazz clazz);
}
