package com.akko.android.repository;

import com.akko.android.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status, Integer> {
    Status findFirstByCode(String code);
}
