package com.akko.android.repository;

import com.akko.android.entity.SubjectSemester;
import com.akko.android.entity.SubjectSlot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface SubjectSlotRepository extends JpaRepository<SubjectSlot, Integer> {
    List<SubjectSlot> findAllByDateGreaterThanEqualAndDateLessThanEqualAndSubjectSemester(Date startDate, Date endDAte, SubjectSemester subjectSemester);
    List<SubjectSlot> findAllBySubjectSemester(SubjectSemester subjectSemester);
}
