package com.akko.android.repository;

import com.akko.android.entity.SubjectSession;
import com.akko.android.entity.SubjectSlot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubjectSessionRepository extends JpaRepository<SubjectSession, Integer> {
    List<SubjectSession> findAllBySlot(SubjectSlot slot);
}
