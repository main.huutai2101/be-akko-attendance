package com.akko.android.repository;

import com.akko.android.entity.Clazz;
import com.akko.android.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Integer> {
    Student findFirstByCode(String code);
    List<Student> findAllByClazz(Clazz clazz);
}
