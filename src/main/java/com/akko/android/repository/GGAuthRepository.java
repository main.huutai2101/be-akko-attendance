package com.akko.android.repository;

import com.akko.android.entity.GGAuth;
import org.springframework.data.jpa.repository.JpaRepository;


public interface GGAuthRepository extends JpaRepository<GGAuth, Integer> {
    GGAuth findFirstByGGAuthId(String GGAuthId);
}
