package com.akko.android.repository;

import com.akko.android.entity.SubjectSlot;
import com.akko.android.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
    Teacher findFirstByCode(String code);
}
