package com.akko.android.repository;

import com.akko.android.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {
    Subject findFirstByCode(String code);
}
