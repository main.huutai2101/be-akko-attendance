package com.akko.android.repository;

import com.akko.android.entity.Major;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MajorRepository extends JpaRepository<Major, Integer> {
    Major findFirstByCode(String code);
}
