package com.akko.android.repository;

import com.akko.android.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Integer> {
    Room findFirstByCode(String code);
}
