package com.akko.android.repository;

import com.akko.android.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NewsRepository extends JpaRepository<News, Integer> {
    List<News> findAllByIdGreaterThan(Integer id);
    List<News> findAllByIdLessThan(Integer id);
    List<News> findAllByTitleLikeOrContentLike(String key1, String key2);
}
