package com.akko.android.repository;

import com.akko.android.entity.Clazz;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClazzRepository extends JpaRepository<Clazz, Integer> {
    Clazz findFirstByCode(String code);
}
