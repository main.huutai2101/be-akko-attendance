package com.akko.android.repository;

import com.akko.android.entity.Semester;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SemesterRepository extends JpaRepository<Semester, Integer> {
    Semester findFirstByCode(String code);
}
