package com.akko.android.exception;

public class DUFileException extends RuntimeException {
    public DUFileException(String message) {
        super(message);
    }
    public DUFileException(String message, Throwable cause) {
        super(message, cause);
    }
}