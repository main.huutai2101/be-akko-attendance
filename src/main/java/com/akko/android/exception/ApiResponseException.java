package com.akko.android.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@ControllerAdvice
@RestController
public class ApiResponseException extends ResponseEntityExceptionHandler {

    /**
     * NOT_FOUND EXCEPTION
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ApiError> handleNotFoundException(NotFoundException ex, HttpServletRequest request) {
        return handle(ex, request, HttpStatus.NOT_FOUND);
    }

    /**
     * CONFLICT EXCEPTION
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(ConflictException.class)
    public final ResponseEntity<ApiError> handleConflictException(ConflictException ex, HttpServletRequest request) {
        return handle(ex, request, HttpStatus.CONFLICT);
    }

    /**
     * Main handle exception
     * @param ex
     * @param request
     * @param httpStatus
     * @return
     */
    private ResponseEntity<ApiError> handle(Exception ex, HttpServletRequest request, HttpStatus httpStatus) {
        ApiError apiError = new ApiError(
                new Date(),
                httpStatus.value(),
                httpStatus.getReasonPhrase(),
                ex.getMessage(),
                request.getRequestURI());
        return new ResponseEntity<>(apiError, httpStatus);
    }
}