package com.akko.android.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DUFileNotFoundException extends RuntimeException {
    public DUFileNotFoundException(String message) {
        super(message);
    }
    public DUFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}