package com.akko.android.service;

import com.akko.android.entity.DUFile;
import com.akko.android.exception.DUFileException;
import com.akko.android.exception.DUFileNotFoundException;
import com.akko.android.property.DUFileProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.Objects;

@Service
public class DUFileService {

    private final String rootPath;
    private final String downloadPath;

    @Autowired
    public DUFileService(DUFileProperties DUFileProperties) {
        this.rootPath = DUFileProperties.getUploadDir();
        this.downloadPath = DUFileProperties.getDownloadUri();
    }

    private Path getPath() {
        return getPath("");
    }
    private Path getPath(String path) {
        Path p = Paths.get(rootPath + path)
                .toAbsolutePath()
                .normalize();

        try {
            Files.createDirectories(p);
            return p;
        } catch (Exception ex) {
            throw new DUFileException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public DUFile storeFile(MultipartFile file) {
        return storeFile(file, "", null);
    }
    public DUFile storeFile(MultipartFile file, String path) {
        return storeFile(file, path, null);
    }
    public DUFile storeFile(MultipartFile file, String path, String renameTo) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        // Rename file with current datetime upload
        String extension = "";
        if (fileName.lastIndexOf(".") != -1) extension = fileName.substring(fileName.lastIndexOf("."));
        if (renameTo == null) fileName = Calendar.getInstance().getTime().getTime() + extension;
        else fileName = renameTo + extension;

        // Check if the file's name contains invalid characters
        if(fileName.contains("..")) {
            throw new DUFileException("Sorry! Filename contains invalid path sequence " + fileName);
        }

        //New thread to storage file
        String finalFileName = fileName;
//        new Thread(() -> {
            try {
                // Copy file to the target location (Replacing existing file with the same name)
                Path targetLocation = getPath(path).resolve(finalFileName);
                String type = file.getContentType();

                assert type != null;
                if (type.contains("image")) compressImg(targetLocation, file);
                else Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            } catch (IOException e) {
                e.printStackTrace();
                throw new DUFileException("Could not store file " + finalFileName + ". Please try again!", e);
            }

//        }).start();



        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(downloadPath + '/')
                .path(path + '/')
                .path(fileName)
                .toUriString();

        return new DUFile(fileName, fileDownloadUri, file.getContentType(), file.getSize());

    }

    private void compressImg(Path targetLocation, MultipartFile file) {
        try {
            final long maxSize = 2097152;
            long size = file.getSize();
            float quality = 0.1f;

            if (size > maxSize) quality = 0.4f;

            File input = new File(targetLocation.toString());
            file.transferTo(input);
            String fileType = targetLocation.toString().substring(targetLocation.toString().lastIndexOf('.')+1);
            BufferedImage image = ImageIO.read(input);

            File output = new File(targetLocation.toString());
            OutputStream out = new FileOutputStream(output);

            ImageWriter writer =  ImageIO.getImageWritersByFormatName(fileType).next();
            ImageOutputStream ios = ImageIO.createImageOutputStream(out);
            writer.setOutput(ios);

            ImageWriteParam param = writer.getDefaultWriteParam();
            if (param.canWriteCompressed()) {
                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                param.setCompressionQuality(quality);
            }

            writer.write(null, new IIOImage(image, null, null), param);
            out.close();
            ios.close();
            writer.dispose();
        } catch (IOException e) {
            e.printStackTrace();
            throw new DUFileException("Could not store file. Please try again!", e);
        }
    }

    public Resource loadFileAsResource(String path) {
        try {
            path = path.replace("%20", " ");
            Path filePath = getPath().resolve(path).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new DUFileNotFoundException("File not found: " + path);
            }
        } catch (MalformedURLException ex) {
            throw new DUFileNotFoundException("File not found: " + path, ex);
        }
    }
}




