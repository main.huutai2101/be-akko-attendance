package com.akko.android.seeder;

import com.akko.android.controller.SubjectSemesterController;
import com.akko.android.dto.SubjectSemesterDto;
import com.akko.android.entity.*;
import com.akko.android.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Configuration
public class GenerateData {
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private ClazzRepository clazzRepository;
    @Autowired
    private SemesterRepository semesterRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private MajorRepository majorRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private GGAuthRepository ggAuthRepository;
    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private SubjectSemesterController subjectSemesterController;

    @Bean
    void statusData() {
        List<Status> statusList = Arrays.asList(
                new Status("ACTIVE","Basic status"),
                new Status("INACTIVE","Basic status"),
                new Status("ATTENDED","Attendance status of students"),
                new Status("ABSENT","Attendance status of students"),
                new Status("NOT_YET","Attendance status of students"),
                new Status("OPEN","Slot status"),
                new Status("CLOSE","Slot status")
        );
        statusRepository.saveAll(statusList);
    }

    @Bean
    void subjectData() {
        List<Subject> subjectList = Arrays.asList(
                new Subject("CPP101b","Basic C++ Programming","(No description)"),
                new Subject("HCI201","Human-Computer Interaction","(No description)"),
                new Subject("PRM391","Mobile Programming","(No description)"),
                new Subject("SWD391","SW Architecture and Design","(No description)"),
                new Subject("SWE102","Introduction to Software Engineering","(No description)"),
                new Subject("LAB221","Desktop Java Lab","(No description)"),
                new Subject("IAA202","Risk Management in Information Systems","(No description)"),
                new Subject("ITE302b","Ethics in IT","(No description)"),
                new Subject("OSP201","Open Source Platform and Network Administration","(No description)"),
                new Subject("CEA201b","Computer Organization and Architecture","(No description)"),
                new Subject("CSI101b","Introduction to Computer Science","(No description)"),
                new Subject("IOT101b","Introduction to Internet of Things","(No description)"),
                new Subject("OSG202","Operating System","(No description)"),
                new Subject("PRF192","Programming Fundamentals","(No description)"),
                new Subject("PRJ321","Web-based Java Applications","(No description)"),
                new Subject("WED201c","Web design","(No description)")
        );
        Status status = statusRepository.findFirstByCode("ACTIVE");
        subjectList.forEach(subject -> subject.setStatus(status));
        subjectRepository.saveAll(subjectList);
    }

    @Bean
    void semesterData() {
        List<Semester> semesterList = Arrays.asList(
                new Semester("SP19","Spring 2019"),
                new Semester("SU19","Summer 2019"),
                new Semester("FA19","Fall 2019"),
                new Semester("SP20","Spring 2020"),
                new Semester("SU20","Summer 2020"),
                new Semester("FA20","Fall 2020")
        );
        Status status = statusRepository.findFirstByCode("ACTIVE");
        semesterList.forEach(semester -> semester.setStatus(status));
        semesterRepository.saveAll(semesterList);
    }

    @Bean
    void clazzData() {
        List<Clazz> clazzList = Arrays.asList(
                new Clazz("SE1301", "(No description)"),
                new Clazz("SE1401", "(No description)"),
                new Clazz("SE1402", "(No description)"),
                new Clazz("SE1403", "(No description)"),
                new Clazz("IA1401", "(No description)"),
                new Clazz("SE1502", "(No description)"),
                new Clazz("SE1502.2", "(No description)"),
                new Clazz("SE1501", "(No description)"),
                new Clazz("SE1501.2", "(No description)"),
                new Clazz("SI1401", "(No description)")
        );
        Status status = statusRepository.findFirstByCode("ACTIVE");
        clazzList.forEach(clazz -> clazz.setStatus(status));
        clazzRepository.saveAll(clazzList);
    }

    @Bean
    void newsData() {
        List<News> list = Arrays.asList(
                new News("FUCT: CUỘC THI LÀM PHIM HOẠT HÌNH \"MÙA HÈ CỦA CÓC\"", "<div id=\"ctl00_mainContent_divContent\"><p>** TRỞ THÀNH ĐẠO DIỄN CÙNG CUỘC THI LÀM PHIM HOẠT HÌNH “MÙA HÈ CỦA CÓC” TẠI HỌC KỲ SUMMER 2020 **<br>\n" +
                        "Với mong muốn tạo sân chơi học thuật cho cộng đồng sinh viên Đại học FPT Cần Thơ và tìm kiếm những gương mặt tiềm năng, đam mê thiết kế đồ họa, có năng khiếu vẽ và sáng tác trong việc làm phim, đặc biệt là phim hoạt hình &nbsp;Cuộc thi làm phim hoạt hình của ĐH FPT Cần Thơ “MÙA HÈ CỦA CÓC” đã chính thức khởi động không giới hạn nội dung, ý tưởng sáng tạo &nbsp;<br>\n" +
                        "&nbsp;- Hình thức dự thi:<br>\n" +
                        "+ Theo nhóm: 3-5 sinh viên.<br>\n" +
                        "+ Thời gian đăng ký đến hết: 25/6/2020<br>\n" +
                        "Mỗi nhóm sẽ trải qua 3 vòng thi: KỊCH BẢN, STORYBOROAD và CÔNG CHIẾU<br>\n" +
                        "<strong>I.&nbsp;Vòng 1: KỊCH BẢN&nbsp;</strong><br>\n" +
                        "Sau khi hết thời gian đăng ký, các đội đăng ký sẽ được tham dự một buổi Coaching với 01 vị đạo diễn phim về cách thức viết kịch bản, lên nội dung, kỹ thuật quay, dựng và biên tập.<br>\n" +
                        "Sau đó, các đội sẽ viết và nộp cho BTC 01 kịch bản, trình bày nội dung theo form mẫu của chương trình và sẽ tham gia báo cáo ý tưởng kịch bản, nhân vật, tình tiết, nội dung cho ban giám khảo. Nội dung trong kịch bản cần trình bày được:<br>\n" +
                        "1. Thông tin đội thi (giới thiệu thành viên, vai trò trong team)<br>\n" +
                        "2. Mô tả chi tiết kịch bản, nội dung câu chuyện;<br>\n" +
                        "3. Nhân vật, tính cách<br>\n" +
                        "4. Cao trào, mẫu thuẫn, cách giải quyết mâu thuẫn, kết thúc kịch bản.<br>\n" +
                        "&nbsp;Các đội gửi kịch bản dự thi gửi về địa chỉ email: fugcct@gmail.com<br>\n" +
                        "&nbsp;Top 10 ý tưởng xuất sắc sẽ vào vòng trong.<br>\n" +
                        "<strong>II. Vòng 2: STORYBROAD</strong><br>\n" +
                        "Các đội vào vòng 2 sẽ được phân công Mentors hỗ trợ về mặt chuyên môn, kỹ thuật để hoàn thiện.<br>\n" +
                        "&nbsp;Hình thức dự thi: &nbsp;Vẽ phác thảo kịch bản, hình ảnh của bộ phim.&nbsp;<br>\n" +
                        "<strong>III. Vòng 3: VÒNG CÔNG CHIẾU</strong><br>\n" +
                        "Các tác phẩm sau khi được Mentor hướng dẫn, các đội sẽ đến phần quay, dựng, kỹ thuật âm thanh, phối nhạc. Bài dự thi hoàn thiện là video phim hoạt hình với thời lượng dưới 3 phút, định đạng .mp4 &nbsp;kích thước FullHD gửi về email cho BTC. Các tác phẩm dự thi sẽ qua 02 vòng chấm điểm như sau:<br>\n" +
                        "- Vòng bình chọn Online: Các đội sẽ nộp video bộ phim của mình về cho BTC, BTC sẽ up lên Fan Page:<br>\n" +
                        "+ Thời gian bình chọn:7/9-12/9<br>\n" +
                        "+ Cách thức và điểm bình chọn: Like = 1 điểm, Share = 3 điểm.<br>\n" +
                        "- Vòng Ban giám khảo chấm điểm: Cơ cấu điểm Bình chọn (30%) + điểm chấm (70%)<br>\n" +
                        "<strong>*** Cơ cấu giải thưởng:</strong><br>\n" +
                        "1 Giải Nhất: 3.000.000 VNĐ<br>\n" +
                        "1 Giải Nhì: 2.000.000 VNĐ<br>\n" +
                        "1 Giải Ba: 1.500.000 VNĐ<br>\n" +
                        "1 Giải Bộ phim được yêu thích nhất: 1,500.000 VNĐ<br>\n" +
                        "05 Giải triển vọng: 500,000 VNĐ<br>\n" +
                        "*** Các đội đăng ký ngay: https://forms.gle/SKDRW4WLT4GT13tx8</p>\n" +
                        "\n" +
                        "<p>-----------------------------------------<br>\n" +
                        "BTC “MÙA HÈ CỦA CÓC” 2020<br>\n" +
                        "&nbsp;Email: fugcct@gmail.com</p>\n" +
                        "\n" +
                        "<p>Hotline: 0939.388.056 (Ms. Vân)</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Final Exam môn HCM201, thi ngày 16/06/2020", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Final Exam môn HCM201, thi ngày 16/06/2020.</p>\n" +
                        "\n" +
                        "<p>Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P. KẾ TOÁN] THÔNG BÁO THU HỌC PHÍ KỲ SUMMER2020_BLOCK 2", "<td>\n" +
                        "\t\t\t\n" +
                        "\t\t\t<p><strong>THÔNG BÁO</strong></p>\n" +
                        "\n" +
                        "\t\t\t<p><em>Vv: Thông báo nộp tiền học phí học kỳ Summer 2020- Block 2&nbsp;hệ đại học chính quy Trường Đại học FPT</em></p>\n" +
                        "\n" +
                        "\t\t\t<ul>\n" +
                        "\t\t\t\t<li>Căn cứ theo Quyết định số 264/QĐ-ĐHFPT ngày 01/03/2019&nbsp;Vv: Quy định tài chính sinh viên năm 2019;</li>\n" +
                        "\t\t\t\t<li>Căn cứ theo Quyết định số QĐ 1410/QĐ-ĐHFPT ngày 09/10/2018 Vv: Ban hành biểu phí các môn học và quy định phí học lại cho sinh viên các hệ đào tạo Trường Đại học FPT;</li>\n" +
                        "\t\t\t\t<li>Căn cứ theo quyết định số 342/QĐ-ĐHFPT Ban hành chính sách hỗ trợ học phí cho thời gian học từ T5/2020 đến hết T8/2020 do ảnh hưởng của dịch bệnh COVID -19 cho sinh viên các hệ đào tạo Trường đại học FPT.</li>\n" +
                        "\t\t\t\t<li>Căn cứ Theo lịch trình học kỳ Summer 2020 Block 2 của Nhà trường.</li>\n" +
                        "\t\t\t</ul>\n" +
                        "\n" +
                        "\t\t\t<p>Phòng Kế Toán&nbsp;thông báo đến quý phụ huynh, sinh viên&nbsp;&nbsp;số tiền học cần nộp như sau:&nbsp;<a href=\"https://drive.google.com/file/d/1PaHwWegP9Y7orVhFgTnL_AUKRd0AHfxh/view?usp=sharing\">https://drive.google.com/file/d/1PaHwWegP9Y7orVhFgTnL_AUKRd0AHfxh/view?usp=sharing</a></p>\n" +
                        "\n" +
                        "\t\t\t<p>Trân trọng.</p>\n" +
                        "\n" +
                        "\t\t\t<p><em><u>Cần Thơ, ngày&nbsp; 17&nbsp;tháng 06&nbsp;năm 2020</u></em></p>\n" +
                        "\t\t\t</td>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Final Exam và lịch thi lại môn JPD141 kỳ SU20", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Final Exam và lịch thi lại môn JPD141:</p>\n" +
                        "\n" +
                        "<p>- Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>- Để xem lịch thi chi tiết, SV truy cập vào FAP, mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Exam/ScheduleExams.aspx\" target=\"_blank\">View exam schedule&nbsp;</a>(Xem lịch thi).</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: NHỮNG ĐIỀU CẦN BIẾT KHI THAM GIA CUỘC THI LÀM PHIM HOẠT HÌNH “MÙA HÈ CỦA CÓC” 2020", "<div id=\"ctl00_mainContent_divContent\"><p>Cuộc thi làm phim hoạt hình “Mùa hè của Cóc 2020” chỉnh thức khởi động và đang thu hút đông đảo sinh viên ĐH FPT Cần Thơ quan tâm, tìm hiểu. Đây là cuộc thi nằm trong chuỗi các hoạt động phát triển kỹ năng cho sinh viên nhà trường, các bạn có đam mê về vẽ, thiết kế mỹ thuật chuyên hay không chuyên,có khả năng sáng tác truyện, phim đều có cơ hội để tham gia. Nghe thì có vẻ khó nhưng kỳ thực cuộc thi làm phim hoạt hình là một sân chơi hoàn toàn hấp dẫn và đầy thú vị. Hôm nay, Ad sẽ chia sẻ những điều cần biết khi các bạn tham gia vào cuộc thi này nhé!</p>\n" +
                        "\n" +
                        "<p>1. Sinh viên không thuộc chuyên ngành thiết kế đồ họa có thể tham gia được không?</p>\n" +
                        "\n" +
                        "<p>Câu trả lời là: ĐƯỢC.</p>\n" +
                        "\n" +
                        "<p>Hiện nay, làm phim hoạt hình bằng công nghệ 2D, 3D đang chiếm lĩnh các cách thức và kỹ thuật làm phim hiện đại. Nhưng, trước đây không có những phần mềm hỗ trợ này các nhà làm phim hoạt hình truyền thống vẫn hoàn toàn có thể mang đến cho người xem những bộ phim đi vào lịch sử. Sinh viên hoàn toàn có thể sử dụng những phương thức khác để tạo ra những kỹ thuật, chuyển động của nhân vật và biến câu chuyện của bạn thành những thướt phim hấp dẫn. Bạn có thể dựng stop motion nặn đất sét, xếp giấy, bản vẽ, flip book(sách lật), sử dụng App hỗ trợ làm phim hoạt hình như: Tooltastic, MovieTools 3…. Khi vào vòng 2 các đội có ý tưởng xuất sắc sẽ được các Mentor hỗ trợ về mặt kỹ thuật, tư vấn về cách thực hiện để các bạn hoàn chỉnh các bài thi của mình.</p>\n" +
                        "\n" +
                        "<p>2. Khi đăng ký tham dự cần phải có đầy đủ kịch bản trước không?</p>\n" +
                        "\n" +
                        "<p>Câu trả lời là: KHÔNG.</p>\n" +
                        "\n" +
                        "<p>Bạn muốn dự thi nhưng chưa có kịch bản rõ ràng, chưa có một cốt truyện cụ thể, nhưng đã có sẵn những ý tưởng trong đầu, bạn đang lo lắng làm sao để biến nó thành một bộ phim cụ thể. Đừng lo lắng! Khi bạn đăng ký tham gia cuộc thi, sẽ tham gia một buổi tập huấn với Đạo diễn làm phim để giúp các bạn có thể định hướng, xây dựng kịch bản rõ ràng. Và đây cũng là một giá trị mà BTC mong muốn đem đến cho các đội tham gia về những kỹ năng đưa ra một ý tưởng, xây dựng nhân vật và lựa chọn tính cách, bối cục, những cao trào và giải quyết mâu thuẫn, tình huống của câu chuyện. Bên cạnh đó, các mentor khi gặp gỡ và trao đổi sẽ định hướng, điều chỉnh những câu chuyện, ý tưởng của các bạn.</p>\n" +
                        "\n" +
                        "<p>Tham gia chưa kịp lấy giải thưởng để đi ăn tẩm bổ bề dọc đã nhận được những kỹ năng và kiến thức hay để bổ bề ngang.</p>\n" +
                        "\n" +
                        "<p>3. Các đội tham gia có được hỗ trợ về chuyên môn và kỹ thuật làm phim không?</p>\n" +
                        "\n" +
                        "<p>Câu trả lời là: CÓ</p>\n" +
                        "\n" +
                        "<p>Đối với các nhóm chưa nắm vững kỹ thuật, BTC sẽ hỗ trợ liên hệ với các Giảng viên có chuyên môn để tư vấn và giúp đỡ các đội hoàn thiện sản phẩm dự thi.</p>\n" +
                        "\n" +
                        "<p>4. Có những công cụ hoặc phần mềm nào giúp hỗ trợ hoàn thành phim hoạt hình?</p>\n" +
                        "\n" +
                        "<p>Câu trả lời là: CÓ.</p>\n" +
                        "\n" +
                        "<p>Ngoài những công cụ chuyên dụng đễ hỗ trợ làm phim và chuyển động chuyên nghiệp như là: Adobe Premiere, Adobe Affter Effects, Adobe Animate….các bạn có thể tham khảo các hướng dẫn theo đường link sau đây:</p>\n" +
                        "\n" +
                        "<p>1. <a href=\"https://www.youtube.com/watch?v=0wEk8KPpqo4&amp;fbclid=IwAR0dibYvXqICC2DTlDTf3DCqlGpffGmR2JZwVqddNIQjpb6hOILfmZyAZG4\" target=\"_blank\">https://www.youtube.com/watch?v=0wEk8KPpqo4</a></p>\n" +
                        "\n" +
                        "<p>2.h<a href=\"https://www.youtube.com/watch?v=2KiMyCkhuIA&amp;fbclid=IwAR3PIfeWAi-2qgFNv02jOzw0j9iTHkgmVEkVbfqQZrShRujN79NLrevv-KU\" target=\"_blank\">ttps://www.youtube.com/watch?v=2KiMyCkhuIA</a></p>\n" +
                        "\n" +
                        "<p>3. <a href=\"https://www.youtube.com/watch?v=1VNTl6n7SOY&amp;fbclid=IwAR230WJymxG6DxWq_cRao9TIH5Ft3LHCnsXVqn-ktkKwnEqqt13KxCybSeY\" target=\"_blank\">https://www.youtube.com/watch?v=1VNTl6n7SOY</a></p>\n" +
                        "\n" +
                        "<p>4. <a href=\"https://www.youtube.com/watch?v=-nRfxX3ysQc&amp;fbclid=IwAR1Y8yMle4vze_gGWmebKntZjdoN7tGyEv_R5tudWs4jvLBAjhTfblzDd9M\" target=\"_blank\">https://www.youtube.com/watch?v=-nRfxX3ysQc</a></p>\n" +
                        "\n" +
                        "<p>5. <a href=\"https://www.youtube.com/watch?v=-nRfxX3ysQc&amp;fbclid=IwAR1gWVpGPr8fXVR4s5rZ3vOUEC_MyJXwGVvEpVhf8VlGbUiYJJU6lAivEdo\" target=\"_blank\">https://www.youtube.com/watch?v=-nRfxX3ysQc</a></p>\n" +
                        "\n" +
                        "<p>6. <a href=\"https://www.youtube.com/watch?v=n47QYAABTmk&amp;fbclid=IwAR3bHbXyzwuRNiJ3r82AP0hgNIQPlm6ueuTjIGh5uwVNE-YuH5eRxOF1cBk\" target=\"_blank\">https://www.youtube.com/watch?v=n47QYAABTmk</a></p>\n" +
                        "\n" +
                        "<p>7. <a href=\"https://www.youtube.com/watch?v=n47QYAABTmk&amp;fbclid=IwAR3bHbXyzwuRNiJ3r82AP0hgNIQPlm6ueuTjIGh5uwVNE-YuH5eRxOF1cBk\" target=\"_blank\">https://www.youtube.com/watch?v=n47QYAABTmk</a></p>\n" +
                        "\n" +
                        "<p>Những ứng dụng này hoàn toàn miễn phí và có trên Play Store, các bạn có thể hoàn toàn tìm thấy những video hướng dẫn trên Youtube và rất dễ dàng thực hiện với tất cả những phiên bản, hình ảnh và chuyển động của các nhân vật.</p>\n" +
                        "\n" +
                        "<p>CÒN CHẦN CHỜ GÌ NỮA MÀ KHÔNG ĐĂNG KÝ NGAY: <a href=\"https://forms.gle/vt1zLTBa3jvsL2qb6\">https://forms.gle/vt1zLTBa3jvsL2qb6</a></p>\n" +
                        "\n" +
                        "<p>CƠ HỘI GẶP GỠ ĐẠO DIỄN PHIM HOẠT HÌNH NỔI TIẾNG</p>\n" +
                        "\n" +
                        "<p>CƠ HỘI THỎA SỨC ĐAM MÊ NGHỆ THUẬT CÙNG “MÙA HÈ CỦA CÓC” 2020.</p>\n" +
                        "\n" +
                        "<p>THỜI HẠN ĐẾN HẾT 25/6/2020.</p>\n" +
                        "\n" +
                        "<p>Phòng CTSV - ĐH FPT Cần Thơ</p>\n" +
                        "\n" +
                        "<p>Hotline: 02923601995 - Email: sro.ct@fe.edu.vn</p>\n" +
                        "\n" +
                        "<p>&nbsp;</p>\n" +
                        "\n" +
                        "<p>&nbsp;</p>\n" +
                        "\n" +
                        "<p>&nbsp;</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Final Exam và lịch thi lại môn MLN101 (lớp BJ1301) kỳ SU20", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Final Exam và lịch thi lại môn MLN101 (lớp BJ1301) kỳ SU20:</p>\n" +
                        "\n" +
                        "<p>- Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>- Để xem lịch thi chi tiết, SV truy cập vào FAP, mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Exam/ScheduleExams.aspx\" target=\"_blank\">View exam schedule&nbsp;</a>(Xem lịch thi).</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Final Exam môn IOT101b, thi ngày 24/06/2020", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Final Exam môn IOT101b, thi ngày 24/06/2020.</p>\n" +
                        "\n" +
                        "<p>Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Resit Exam môn MLN101 (Lớp BJ1301), thi ngày 26/06/2020", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Resit Exam môn MLN101 (Lớp BJ1301), thi ngày 26/06/2020.</p>\n" +
                        "\n" +
                        "<p>Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: CUỘC THI MÙA HÈ CỦA CÓC 2020 THÔNG BÁO GIA HẠN THỜI GIAN ĐĂNG KÝ ĐẾN 5/7/2020", "<div id=\"ctl00_mainContent_divContent\"><p>“MÙA HÈ CỦA CÓC” ĐÃ NHẬN ĐƯỢC 15 Ý TƯỞNG HẤP DẪN VÀ MỚI LẠ THAM GIA DỰ THI</p>\n" +
                        "\n" +
                        "<p>Sau hơn 02 tuần khởi động, cuộc thi làm phim hoạt hình “Mùa hè của Cóc 2020” đã nhận được khá nhiều sự quan tâm và tìm hiểu, cuộc thi nhanh chóng thu hút được 15 ý tưởng kịch bản phim hoạt hình thú vị và mới lạ của rất nhiều các đội nhóm sinh viên ĐH FPT Cần Thơ đến từ nhiều chuyên ngành, lĩnh vực khác nhau.</p>\n" +
                        "\n" +
                        "<p>Mùa hè của Cóc 2020 là một cuộc thi đầy sáng tạo và thử thách dành cho những gương mặt tiềm năng, đam mê thiết kế đồ họa, các cá nhân chuyên hoặc không chuyên trong lĩnh vực thiết kế phim, các bạn có năng khiếu vẽ và sáng tác các kịch bản phim, đặc biệt là phim hoạt hình. Đây là một Format vô cùng mới lạ và lần đầu tiên được tổ chức tranh tài tại ĐH FPT Cần Thơ. Để tiếp tục tạo cơ hội cho các bạn sinh viên được thỏa sức sáng tạo và tiếp cận những hoạt động kỹ năng mới lạ, BTC xin thông báo gia hạn thời gian đăng ký đến ngày 5/7/2020.</p>\n" +
                        "\n" +
                        "<p>Link đăng ký quen thuộc: https://forms.gle/vt1zLTBa3jvsL2qb6</p>\n" +
                        "\n" +
                        "<p>Hết thời gian nêu trên, các ý tưởng xuất sắc nhất, các đội tham dự sáng tạo và hấp dẫn nhất sẽ được gặp gỡ 1 đạo diễn phim hoạt hình nổi tiếng để tham dự buổi Coaching nhằm củng cố kịch bản, nhân vật, cá tính và bối cảnh bộ phim giúp các đội tự tin và hoàn thiện tác phẩm của mình.</p>\n" +
                        "\n" +
                        "<p>NHANH TAY ĐĂNG KÝ – THỬ TÀI LÀM ĐẠO DIỄN!!!</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P. Kế Toán ] V/v Sinh Viên Chuyển Khoản Chưa Có Thông Tin", "<div id=\"ctl00_mainContent_divContent\"><p>P. Kế Toán gửi đến các bạn sinh viên Trường Đại Học FPT Cần Thơ.</p>\n" +
                        "\n" +
                        "<p>Hiện tại các giao dịch chuyển khoản của sinh viên:&nbsp;</p>\n" +
                        "\n" +
                        "<p>1. Giấy xác nhận sinh viên.</p>\n" +
                        "\n" +
                        "<p>2. Phí học lại.</p>\n" +
                        "\n" +
                        "<p>3. Phí phạt thư viện</p>\n" +
                        "\n" +
                        "<p>Nhiều bạn sinh viên chuyển khoản không ghi nhận : HỌ TÊN + MÃ SINH VIÊN</p>\n" +
                        "\n" +
                        "<p>Kế toán không thể xác định thông tin để xuất hóa đơn, và cập nhật kịp thời cho các bạn. Qua thông báo này, các bạn sinh viên nếu đã chuyển khoản sau 2 ngày làm việc chưa nhận được hóa đơn điện tử vui lòng liên hệ kế toán- thu ngân&nbsp; ( P. Tuyển sinh) , để được&nbsp;giải quyết, và xuất hóa đơn bổ sung.</p>\n" +
                        "\n" +
                        "<p>Trân trọng.</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [ĐĂNG KÝ LỚP KỸ NĂNG GIAO TIẾP HIỆU QUẢ THÁNG 7/2020]", "<div id=\"ctl00_mainContent_divContent\"><p><strong>[THAM GIA NGAY LỚP KỸ NĂNG GIAO TIẾP HIỆU QUẢ THÁNG 7/2020]</strong></p>\n" +
                        "\n" +
                        "<p>Giao tiếp tốt là tiền đề cho những mối quan hệ trong cuộc sống, là khởi đầu cho những thành công trong học tập, công việc, trong các cuộc trao đổi, thương thảo và hơn cả là giúp cho cuộc sống của bạn có thêm những màu sắc và thông điệp hơn. Nghệ thuật giao tiếp sẽ giúp bạn mở rộng và tiếp nhận thêm nhiều thông tin, xây dựng một hình ảnh tinh tế và gia tăng giá trị hấp dẫn cho chính bản thân bạn trong tất cả các cuộc giao tiếp, giúp bạn tạo ấn tượng với đối phương, khiến bạn trở nên hấp dẫn và thú vị.</p>\n" +
                        "\n" +
                        "<p>Nhưng không ít bạn sẽ chưa biết cách giao tiếp và có những hành động cho phù hợp, bạn còn ngại ngùng, rụt rè khi gặp người lạ hoặc vào môi trường mới, lúng túng không biết thể hiện bản thân và xử lý tình huống một cách khéo léo, khó khăn khi xây dựng mối quan hệ mới….Nếu bạn nắm được những kỹ năng giao tiếp thông minh và hiệu quả&nbsp;thì bạn sẽ biết cách tạo sức hút cá nhân làm nền tảng cho việc giao tiếp. <strong><em>Hãy đến với khóa học&nbsp;\"Kỹ năng giao tiếp hiệu quả\" để được:</em></strong></p>\n" +
                        "\n" +
                        "<p>FNhận thức được tầm quan trọng của giao tiếp và kỹ năng giao tiếp thông minh, Nắm được các hình thức giao tiếp hiện nay, nhận ra những nguyên nhân và khắc phục những vấn đề trong giao tiếp cá nhân.</p>\n" +
                        "\n" +
                        "<p>FNắm được các mẹo giao tiếp thông minh, tinh tế trong các tình huống trực tiếp hoặc gián tiếp (điện thoại, email…) và hiệu quả ngay từ lần đầu gặp để tạo ấn tượng tốt tới đối phương, biết lắng nghe và chia sẻ một cách hiệu quả, chủ động trong mọi tình huống.</p>\n" +
                        "\n" +
                        "<p>FThấu hiểu bản thân và đối tác thông qua các mẹo giao tiếp và nhận biết của bản thân</p>\n" +
                        "\n" +
                        "<p>TKhóa học với sự hướng dẫn giảng dạy trực tiếp của Cô Ngô Thị Thúy An- Giảng viên ĐH FPT Cần Thơ sẽ giúp bạn có những kỹ năng và cách ứng xử để tự tin trong giao tiếp với bạn bè, đồng nghiệp, người mới quen và cả người thân một cách thông minh, hiệu quả và tạo được sức ảnh hưởng cá nhân với tất cả mọi người.</p>\n" +
                        "\n" +
                        "<p><strong>THÔNG TIN KHÓA HỌC:</strong></p>\n" +
                        "\n" +
                        "<p>- Thời gian học: 03 buổi tối từ 18h đến 21h các ngày Thứ Hai (13/7), Thứ Tư (15/7), Thứ Sáu (17/7)</p>\n" +
                        "\n" +
                        "<p>- Địa điểm: Phòng 315 – ĐH FPT Cần Thơ.</p>\n" +
                        "\n" +
                        "<p>- Thời gian đăng ký đến <strong>12h ngày thứ 7 (11/7/2020)</strong></p>\n" +
                        "\n" +
                        "<p>Chắc chắn rằng sau khi hoàn thành khóa học này bạn sẽ sớm làm chủ được cuộc giao tiếp với bất kỳ đối phương nào, tăng sự tự tin và chinh phục được cuộc giao tiếp một cách thật thông minh, tinh tế và khéo léo!</p>\n" +
                        "\n" +
                        "<p>Đăng ký ngay tại: <a href=\"https://forms.gle/ph8ETVTGaZMbJ8ci7\">https://forms.gle/ph8ETVTGaZMbJ8ci7</a></p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Final Exam và lịch thi lại các môn JPD325, SSG101 (lóp JL14) block 1 kỳ SU20", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Final Exam và lịch thi lại các môn JPD325, SSG101 (lớp JL14) block 1 kỳ SU20:</p>\n" +
                        "\n" +
                        "<p>- Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>- Để xem lịch thi chi tiết, SV truy cập vào FAP, mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Exam/ScheduleExams.aspx\" target=\"_blank\">View exam schedule&nbsp;</a>(Xem lịch thi).</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Resit Exam môn JPD325 và SSG101", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Resit Exam môn JPD325 (thi ngày 09/07/2020), môn SSG101 (thi ngày 10/07/2020).</p>\n" +
                        "\n" +
                        "<p>Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Final Exam và lịch thi lại môn CPP101b kỳ SU20", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Final Exam và lịch thi lại môn CPP101b&nbsp;block 1 kỳ SU20:</p>\n" +
                        "\n" +
                        "<p>- Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>- Để xem lịch thi chi tiết, SV truy cập vào FAP, mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Exam/ScheduleExams.aspx\" target=\"_blank\">View exam schedule&nbsp;</a>(Xem lịch thi).</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT"),
                new News("FUCT: [P.ĐT] Thông báo điểm thi Final Exam và lịch thi lại các môn ENT203, ENT303, ENT403, ENT503 block 1 kỳ SU20", "<div id=\"ctl00_mainContent_divContent\"><p>Các em sinh viên thân mến,</p>\n" +
                        "\n" +
                        "<p>Phòng Đào tạo thông báo đã có điểm thi Final Exam và lịch thi lại các môn ENT203, ENT303, ENT403, ENT503 block 1 kỳ SU20:</p>\n" +
                        "\n" +
                        "<p>- Để xem điểm chi tiết, SV truy cập vào FAP, chọn mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Grade/StudentGrade.aspx\">Mark Report</a>&nbsp;(Báo cáo điểm)</p>\n" +
                        "\n" +
                        "<p>- Để xem lịch thi chi tiết, SV truy cập vào FAP, mục&nbsp;<a href=\"http://fap.fpt.edu.vn/Exam/ScheduleExams.aspx\" target=\"_blank\">View exam schedule&nbsp;</a>(Xem lịch thi).</p>\n" +
                        "\n" +
                        "<p>Chúc các em học tập tốt!</p>\n" +
                        "\n" +
                        "<p>P.TC&amp;QLDT FU-CT: Email:&nbsp;<a href=\"mailto:aca.ct@fe.edu.vn\">aca.ct@fe.edu.vn</a>. Điện thoại:&nbsp;(0292)360 1994</p>\n" +
                        "</div>", "ThaoVT")
        );

        newsRepository.saveAll(list);
    }

    @Bean
    void majorData() {
        List<Major> majorList = Arrays.asList(
                new Major("EN","English"),
                new Major("JP","Japanese"),
                new Major("KR","Korean"),
                new Major("SE","Software Engineering"),
                new Major("IA","Information Assurance"),
                new Major("GD","Graphic Design"),
                new Major("BA","Business Administration"),
                new Major("IB","International Business"),
                new Major("HM","Hospitality Management"),
                new Major("MC","Multimedia Communication"),
                new Major("TM","Tourism Management"),
                new Major("CS","Common Subject")
        );
        Status status = statusRepository.findFirstByCode("ACTIVE");
        majorList.forEach(major -> major.setStatus(status));
        majorRepository.saveAll(majorList);
    }

    @Bean
    void roleData() {
        List<Role> roleList = Arrays.asList(
                new Role("Admin","Role for Administrator"),
                new Role("Teacher","Role for Teacher"),
                new Role("Academy","Role for Academy")
        );
        Status status = statusRepository.findFirstByCode("ACTIVE");
        roleList.forEach(role -> role.setStatus(status));
        roleRepository.saveAll(roleList);
    }

    @Bean
    void roomData() {
        List<Room> roomList = Arrays.asList(
                new Room("B201","B2",30),
                new Room("B202","B2",30),
                new Room("B203","B2",30),
                new Room("B204","B2",30),
                new Room("B205","B2",30),
                new Room("B206","B2",30),
                new Room("B207","B2",30),
                new Room("B208","B2",30),
                new Room("B209","B2",30),
                new Room("B210","B2",30),
                new Room("B211","B2",30),
                new Room("B212","B2",30),
                new Room("B213","B2",30),
                new Room("B214","B2",30),
                new Room("B215","B2",30),
                new Room("B301","B3",30),
                new Room("B302","B3",30),
                new Room("B303","B3",30),
                new Room("B304","B3",30),
                new Room("B305","B3",30),
                new Room("B306","B3",30),
                new Room("B307","B3",30),
                new Room("B308","B3",30),
                new Room("B309","B3",30),
                new Room("B310","B3",30),
                new Room("B311","B3",30),
                new Room("B312","B3",30),
                new Room("B313","B3",30),
                new Room("B314","B3",30),
                new Room("B315","B3",30),
                new Room("B318","B3",30)
        );
        Status s = statusRepository.findFirstByCode("ACTIVE");
        roomList.forEach(room -> room.setStatus(s));
        roomRepository.saveAll(roomList);
    }

    @Bean
    void teacherData() {
        List<Teacher> teacherList = Arrays.asList(
                new Teacher("KhanhVH","Võ Hồng Khanh","01/01/1990",0,"Cần Thơ","0912345678","KhanhVH@fpt.edu.vn","2018","Web Application", "http://localhost:8000/download/Teachers/KhanhVH.jpg", "http://localhost:8000/download/Covers/KhanhVH.jpg"),
                new Teacher("HuongLH3","Lương Hoàng Hướng","01/01/1990",0,"Cần Thơ","0912345678","HuongLH3@fpt.edu.vn","2018","Desktop Application", "http://localhost:8000/download/Teachers/HuongLH3.jpg", "http://localhost:8000/download/Covers/HuongLH3.jpg"),
                new Teacher("DaQL","Quách Luyl-Đa","01/01/1990",0,"Cần Thơ","0912345678","DaLQ@fpt.edu.vn","2018","Mobile Programming","http://localhost:8000/download/Teachers/DaLQ.jpg", "http://localhost:8000/download/Covers/DaLQ.jpg")
        );
        Status status = statusRepository.findFirstByCode("ACTIVE");
        teacherList.forEach(teacher -> teacher.setStatus(status));
        teacherRepository.saveAll(teacherList);
    }

    @Bean
    void studentData() {
        Status status = statusRepository.findFirstByCode("ACTIVE");

        Major se = majorRepository.findFirstByCode("SE");
        List<Student> se1301 = Arrays.asList(
                new Student("CE130009","Nguyễn Đình Khôi","21/2/1999",0,"Tuyên Quang","0969114835","khoindce130009@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/khoindce130009.png"),
                new Student("CE130013","Kiên Hà Ngọc Tấn","10/5/1999",0,"Ninh Thuận","0837102010","tankhnce130013@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/tankhnce130013.png"),
                new Student("CE130024","Lê Nguyễn Hữu Tài","18/5/1999",0,"Bến Tre","0868286745","tailnhce130024@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/tailnhce130024.png"),
                new Student("CE130040","Võ Tấn Anh","15/2/1999",0,"Sóc Trăng","0859185411","anhvtce130040@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/anhvtce130040.png"),
                new Student("CE130056","Nguyễn Đức Huy Hùng","22/6/1999",0,"Đắk Lắk","0581762509","hungndhce130056@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/hungndhce130056.png"),
                new Student("CE130071","Nguyễn Đình Khôi","28/9/1999",0,"Quảng Trị","0363615907","khoindce130071@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/khoindce130071.png"),
                new Student("CE130094","Đinh Hoàng Tuấn","1/5/1999",0,"Bình Dương","0340400705","tuandhce130094@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/tuandhce130094.png"),
                new Student("CE130097","Phạm Võ Anh Quốc","12/7/1999",0,"Kiên Giang","0901641850","quocpvace130097@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/quocpvace130097.png"),
                new Student("CE130100","Lê Đức An","24/11/1999",0,"Ninh Bình","0336399302","anldce130100@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/anldce130100.png"),
                new Student("CE130109","Phạm Chí Đức","17/10/1999",0,"Khánh Hòa","0337202585","ducpcce130109@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/ducpcce130109.png"),
                new Student("CE130114","Hồ Duy Anh","26/10/1999",0,"Phú Yên","0930866852","anhhdce130114@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/anhhdce130114.png"),
                new Student("CE130157","Đỗ Trung Tín","28/2/1999",0,"Bình Phước","0858243612","tindtce130157@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/tindtce130157.png"),
                new Student("CE130160","Phạm Duy","2/4/1999",0,"Đồng Nai","0761205602","duypce130160@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/duypce130160.png"),
                new Student("CE130169","Trần Thanh Trọng","10/12/1999",0,"Trà Vinh","0810493957","trongttce130169@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/trongttce130169.png"),
                new Student("CE130172","Phạm Hồng Đỉnh","15/10/1999",0,"Bắc Ninh","0928769385","dinhphce130172@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/dinhphce130172.png"),
                new Student("CE130192","Hà Văn Ngoan","26/7/1999",0,"Trà Vinh","0925027038","ngoanhvce130192@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/ngoanhvce130192.png"),
                new Student("CE130204","Lý Dương","24/11/1999",0,"Bến Tre","0860028200","duonglce130204@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/duonglce130204.png"),
                new Student("CE130205","Nguyễn Quốc Đạt","28/1/1999",0,"Nam Định","0984433024","datnqce130205@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/datnqce130205.png"),
                new Student("CE130222","Huỳnh Đình Mẫn","1/4/1999",0,"Bình Phước","0383547860","manhdce130222@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/manhdce130222.png"),
                new Student("CE130226","Hà Quang Trình","7/2/1999",0,"An Giang","0706904050","trinhhqce130226@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/trinhhqce130226.png"),
                new Student("CE130234","Trần Huỳnh Long","1/8/1999",0,"Vĩnh Long","0365688117","longthce130234@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/longthce130234.png"),
                new Student("CE130250","Nguyễn Văn Ngọc","19/11/1999",0,"Khánh Hòa","0840102498","ngocnvce130250@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/ngocnvce130250.png"),
                new Student("CE130289","Lâm Minh Khang","15/3/1999",0,"Vĩnh Long","0832883122","khanglmce130289@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/khanglmce130289.png"),
                new Student("CE130309","Nguyễn Đông Hưng","13/9/1999",0,"Đồng Tháp","0707561066","hungndce130309@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/hungndce130309.png"),
                new Student("CE130319","Phan Tấn Phát","17/2/1999",0,"Tây Ninh","0361807609","phatptce130319@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/phatptce130319.png"),
                new Student("CE130325","Nguyễn Thị Thanh Huyền","19/12/1999",1,"Hải Phòng","0703622945","huyennttce130325@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/huyennttce130325.png"),
                new Student("CE130336","Phan Minh Đức","5/1/1999",0,"Vĩnh Phúc","0762604436","ducpmce130336@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/ducpmce130336.png"),
                new Student("CE130359","Đỗ Thành Đạt","10/12/1999",0,"Lâm Đồng","0326210517","datdtce130359@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/datdtce130359.png"),
                new Student("CE130370","Phạm Bảo Lợi","2/1/1999",0,"Điện Biên","0380044954","loipbce130370@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/loipbce130370.png"),
                new Student("CE130427","Đặng Bửu Hòa","19/3/1999",0,"Ninh Thuận","0937320562","hoadbce130427@fpt.edu.vn","http://localhost:8000/download/Students/SE1301/hoadbce130427.png")
        );
        Clazz se1301class = clazzRepository.findFirstByCode("SE1301");
        se1301.forEach(student -> {
            student.setMajor(se);
            student.setClazz(se1301class);
            student.setStatus(status);
        });
        studentRepository.saveAll(se1301);



        List<Student> se1401 = Arrays.asList(
                new Student("CE130132","Nguyễn Hoàng Hữu Trọng","28/4/1999",0,"Lào Cai","0912843831","trongnhhce130132@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/trongnhhce130132.png"),
                new Student("CE130143","Võ Quang Huy","15/11/1999",0,"Bà Rịa - Vũng Tàu","0869696387","huyvqce130143@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/huyvqce130143.png"),
                new Student("CE140007","Ngô Nguyễn Hồng Hải Long","13/12/1999",0,"Tây Ninh","0973660196","LongNNHHCE140007@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/LongNNHHCE140007.png"),
                new Student("CE140019","Trịnh Đình Quang","18/12/1999",0,"Hoà Bình","0910159096","QuangTDCE140019@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/QuangTDCE140019.png"),
                new Student("CE140033","Hà Thúc Huỳnh Duy","13/12/1999",0,"Tiền Giang","0837791947","DuyHTHCE140033@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/DuyHTHCE140033.png"),
                new Student("CE140037","Quản Đức Lộc","13/3/1999",0,"Bạc Liêu","0822707594","LocQDCE140037@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/LocQDCE140037.png"),
                new Student("CE140069","Nguyễn Hào Phú","28/7/1999",0,"Đồng Nai","0833118644","PhuNHCE140069@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/PhuNHCE140069.png"),
                new Student("CE140085","Trần Minh Thắng","24/1/1999",0,"Quảng Ninh","0365061420","ThangTMCE140085@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/ThangTMCE140085.png"),
                new Student("CE140102","Phan Lê Trọng Nghĩa","27/2/1999",0,"Bình Dương","0909322526","NghiaPLTCE140102@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/NghiaPLTCE140102.png"),
                new Student("CE140131","Đặng Minh Thuận","15/5/1999",0,"Lạng Sơn","0815227905","ThuanDMCE140131@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/ThuanDMCE140131.png"),
                new Student("CE140156","Vũ Tuấn Minh","17/1/1999",0,"Hậu Giang","0561194910","MinhVTCE140156@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/MinhVTCE140156.png"),
                new Student("CE140165","Nguyễn Đăng Khoa","24/8/1999",0,"Hậu Giang","0351054621","KhoaNDCE140165@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/KhoaNDCE140165.png"),
                new Student("CE140175","Bùi Đình Gia Huy","12/4/1999",0,"Hải Phòng","0843700754","HuyBDGCE140175@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/HuyBDGCE140175.png"),
                new Student("CE140196","Nguyễn Đức Tông","16/7/1999",0,"Bình Thuận","0969922222","TongNDCE140196@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/TongNDCE140196.png"),
                new Student("CE140219","Nguyễn Hữu Lý","6/9/1999",0,"Thanh Hóa","0799008715","LyNHCE140219@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/LyNHCE140219.png"),
                new Student("CE140273","Tất Huỳnh Anh Khôi","9/1/1999",0,"Hưng Yên","0765965785","KhoiTHACE140273@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/KhoiTHACE140273.png"),
                new Student("CE140289","Trần Ngọc Mẫn Huy","21/12/1999",0,"Điện Biên","0888288496","HuyTNMCE140289@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/HuyTNMCE140289.png"),
                new Student("CE140300","Lưu Tiến Minh","26/2/1999",0,"Yên Bái","0937364099","MinhLTCE140300@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/MinhLTCE140300.png"),
                new Student("CE140313","Ngô Tường Vinh","23/10/1999",0,"Phú Thọ","0764528038","VinhNTCE140313@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/VinhNTCE140313.png"),
                new Student("CE140337","Võ Anh Nhiều","1/3/1999",0,"Tây Ninh","0368128634","NhieuVACE140337@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/NhieuVACE140337.png"),
                new Student("CE140341","Đinh Công Toại","19/3/1999",0,"Quảng Ngãi","0391103009","ToaiDCCE140341@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/ToaiDCCE140341.png"),
                new Student("CE140343","Đặng Lê Thành Nam","19/3/1999",0,"Trà Vinh","0342784570","NamDLTCE140343@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/NamDLTCE140343.png"),
                new Student("CE140350","Nguyễn Thúy Hiền","23/11/1999",1,"Hậu Giang","0900195479","HienNTCE140350@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/HienNTCE140350.png"),
                new Student("CE140397","Vương Đình Nguyên","24/7/1999",0,"Bà Rịa - Vũng Tàu","0906073617","NguyenVDCE140397@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/NguyenVDCE140397.png"),
                new Student("CE140401","Dương Trí Tín","28/4/1999",0,"Quảng Ngãi","0789068119","TinDTCE140401@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/TinDTCE140401.png"),
                new Student("CE140417","Nguyễn Văn Khương","17/12/1999",0,"Thái Bình","0332810929","KhuongNVCE140417@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/KhuongNVCE140417.png"),
                new Student("CE140420","Huỳnh Nhựt Tân","20/5/1999",0,"Lâm Đồng","0881168615","TanHNCE140420@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/TanHNCE140420.png"),
                new Student("CE140433","Võ Phan Bảo Long","22/11/1999",0,"Tuyên Quang","0708758432","LongVPBCE140433@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/LongVPBCE140433.png"),
                new Student("CE140435","Nguyễn Thị Diễm Hương","1/3/1999",1,"Đà Nẵng","0332229185","HuongNTDCE140435@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/HuongNTDCE140435.png"),
                new Student("CE140443","Nguyễn Phạm Hữu Tài","10/11/1999",0,"Tiền Giang","0769332852","TaiNPHCE140443@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/TaiNPHCE140443.png"),
                new Student("CE140467","Diệp Trí Thành","22/2/1999",0,"Bạc Liêu","0931987004","ThanhDTCE140467@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/ThanhDTCE140467.png"),
                new Student("CE140469","Lê Đức Hòa","22/9/1999",0,"Quảng Ninh","0925908826","HoaLDCE140469@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/HoaLDCE140469.png"),
                new Student("CE140520","Trần Nguyễn Quốc Huy","2/4/1999",0,"Hải Phòng","0566387539","HuyTNQCE140520@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/HuyTNQCE140520.png"),
                new Student("CE140539","Nguyễn Tân Đại Phát","6/5/1999",0,"Hà Giang","0937232202","PhatNTDCE140539@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/PhatNTDCE140539.png"),
                new Student("CE140548","Ngô Hoàn Tâm Huy","21/3/1999",0,"Kiên Giang","0855183220","HuyNHTCE140548@fpt.edu.vn","http://localhost:8000/download/Students/SE1401/HuyNHTCE140548.png")
        );
        Clazz se1401class = clazzRepository.findFirstByCode("SE1401");
        se1401.forEach(student -> {
            student.setMajor(se);
            student.setClazz(se1401class);
            student.setStatus(status);
        });
        studentRepository.saveAll(se1401);


        List<Student> se1402 = Arrays.asList(
                new Student("CE130047","Nguyễn Lê Yến Vy","4/2/1999",1,"Quảng Ngãi","0766569146","vynlyce130047@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/vynlyce130047.png"),
                new Student("CE130199","Nguyễn Hữu Toàn","1/4/1999",0,"Quảng Nam","0588155047","toannhce130199@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/toannhce130199.png"),
                new Student("CE140023","Nguyễn Văn Đăng","9/6/1999",0,"Hà Nội","0827689203","DangNVCE140023@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/DangNVCE140023.png"),
                new Student("CE140066","Phạm Tiến Huy","4/2/1999",0,"Nam Định","0706416664","HuyPTCE140066@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/HuyPTCE140066.png"),
                new Student("CE140071","Lê Minh Nghĩa","6/5/1999",0,"Kon Tum","0760599093","NghiaLMCE140071@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/NghiaLMCE140071.png"),
                new Student("CE140136","Đỗ Thành Tài","10/1/1999",0,"Điện Biên","0883928902","TaiDTCE140136@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/TaiDTCE140136.png"),
                new Student("CE140150","Nguyễn Tấn Lộc","22/7/1999",0,"Quảng Ngãi","0935772882","LocNTCE140150@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/LocNTCE140150.png"),
                new Student("CE140197","Huỳnh Nhật Minh","19/12/1999",0,"Lào Cai","0883759310","MinhHNCE140197@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/MinhHNCE140197.png"),
                new Student("CE140257","Tôn Tú Trinh","6/4/1999",1,"Bến Tre","0388499407","TrinhTTCE140257@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/TrinhTTCE140257.png"),
                new Student("CE140276","Lê Hữu Tài","9/8/1999",0,"Lai Châu","0905813203","TaiLHCE140276@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/TaiLHCE140276.png"),
                new Student("CE140278","Nguyễn Vĩnh Lạp","28/7/1999",0,"Hậu Giang","0327497616","LapNVCE140278@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/LapNVCE140278.png"),
                new Student("CE140329","Võ Hoàng Duy Thái","8/3/1999",0,"Bạc Liêu","0810174479","ThaiVHDCE140329@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/ThaiVHDCE140329.png"),
                new Student("CE140331","Trần Trung Định","20/6/1999",0,"Hà Giang","0964721348","DinhTTCE140331@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/DinhTTCE140331.png"),
                new Student("CE140372","Nguyễn Hữu Trọng","9/1/1999",0,"Thái Bình","0925888291","TrongNHCE140372@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/TrongNHCE140372.png"),
                new Student("CE140374","Vũ Đức Huy","15/1/1999",0,"Sóc Trăng","0390665235","HuyVDCE140374@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/HuyVDCE140374.png"),
                new Student("CE140408","Nguyễn Văn Ngoan","27/3/1999",0,"Tuyên Quang","0367726834","NgoanNVCE140408@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/NgoanNVCE140408.png"),
                new Student("CE140412","Nguyễn Trung Hiển","7/12/1999",0,"Phú Thọ","0933400677","HienNTCE140412@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/HienNTCE140412.png"),
                new Student("CE140462","Nguyễn Khắc Huy","22/12/1999",0,"Đắk Lắk","0342789562","HuyNKCE140462@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/HuyNKCE140462.png"),
                new Student("CE140504","Nguyễn Hoàng Gia Khánh","26/12/1999",0,"An Giang","0912654365","KhanhNHGCE140504@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/KhanhNHGCE140504.png"),
                new Student("CE140505","Bùi Minh Triết","26/11/1999",0,"Bắc Kạn","0858224520","TrietBMCE140505@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/TrietBMCE140505.png"),
                new Student("CE140508","Nguyễn Minh Quang","27/4/1999",0,"Hải Dương","0769946554","QuangNMCE140508@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/QuangNMCE140508.png"),
                new Student("CE140527","Nguyễn Phát Tài","24/5/1999",0,"Hưng Yên","0362793002","TaiNPCE140527@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/TaiNPCE140527.png"),
                new Student("CE140563","Phan Thanh Liêm","24/8/1999",0,"Đà Nẵng","0939063283","LiemPTCE140563@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/LiemPTCE140563.png"),
                new Student("CE140591","Lý Huỳnh Trọng Ân","23/5/1999",0,"Phú Yên","0785388950","AnLHTCE140591@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/AnLHTCE140591.png"),
                new Student("CE140596","Nguyễn Hữu Duy","27/11/1999",0,"Tuyên Quang","0922290855","DuyNHCE140596@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/DuyNHCE140596.png"),
                new Student("CE140626","Tạ Nguyễn Thúy Vy","2/8/1999",1,"Cần Thơ","0765700722","VyTNTCE140626@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/VyTNTCE140626.png"),
                new Student("CE140640","Trương Thanh Toàn","1/12/1999",0,"Đà Nẵng","0841995876","ToanTTCE140640@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/ToanTTCE140640.png"),
                new Student("CE140641","Lưu Phước Sang","4/11/1999",0,"Nam Định","0851618903","SangLPCE140641@fpt.edu.vn","http://localhost:8000/download/Students/SE1402/SangLPCE140641.png")
        );
        Clazz se1402class = clazzRepository.findFirstByCode("SE1402");
        se1402.forEach(student -> {
            student.setMajor(se);
            student.setClazz(se1402class);
            student.setStatus(status);
        });
        studentRepository.saveAll(se1402);


        List<Student> se1403 = Arrays.asList(
                new Student("CE130235","Huỳnh Lê Hải Đăng","13/2/1999",0,"Hải Phòng","0791487599","danghlhce130235@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/danghlhce130235.png"),
                new Student("CE130302","Thạch Quí","6/1/1999",0,"Quảng Ngãi","0835092976","quitce130302@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/quitce130302.png"),
                new Student("CE130438","Tăng Minh Tín","2/9/1999",0,"Nam Định","0930869408","TinTMCE130438@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/TinTMCE130438.png"),
                new Student("CE140044","Lê Thành Nhân","23/10/1999",0,"Kiên Giang","0885272461","NhanLTCE140044@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/NhanLTCE140044.png"),
                new Student("CE140059","Nguyễn Hoàng Huệ Nhân","22/1/1999",1,"Vĩnh Phúc","0826480018","NhanNHHCE140059@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/NhanNHHCE140059.png"),
                new Student("CE140079","Bạch Nguyễn Phúc Thịnh","18/5/1999",0,"Kiên Giang","0356614194","ThinhBNPCE140079@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/ThinhBNPCE140079.png"),
                new Student("CE140110","Nguyễn Quốc Toàn","14/12/1999",0,"Lạng Sơn","0790670947","ToanNQCE140110@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/ToanNQCE140110.png"),
                new Student("CE140122","Huỳnh Thị Nhiên","13/6/1999",1,"Quảng Ngãi","0984588143","NhienHTCE140122@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/NhienHTCE140122.png"),
                new Student("CE140124","Nguyễn Quốc An","25/5/1999",0,"An Giang","0822712917","AnNQCE140124@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/AnNQCE140124.png"),
                new Student("CE140143","Hứa Quốc Vinh","27/3/1999",0,"Điện Biên","0581549167","VinhHQCE140143@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/VinhHQCE140143.png"),
                new Student("CE140149","Trương Thị Thanh Xuân","18/9/1999",1,"Bình Định","0783419098","XuanTTTCE140149@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/XuanTTTCE140149.png"),
                new Student("CE140179","Phạm Quốc Nghị","23/1/1999",0,"Cà Mau","0852082297","NghiPQCE140179@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/NghiPQCE140179.png"),
                new Student("CE140224","Nguyễn Vũ Khang","16/1/1999",0,"Quảng Ninh","0331940081","KhangNVCE140224@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/KhangNVCE140224.png"),
                new Student("CE140252","Tiêu Hà Anh Khôi","19/12/1999",0,"Hoà Bình","0909851181","KhoiTHACE140252@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/KhoiTHACE140252.png"),
                new Student("CE140311","Nguyễn Trần Quang Hiển","22/7/1999",0,"Cần Thơ","0977461511","HienNTQCE140311@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/HienNTQCE140311.png"),
                new Student("CE140320","Huỳnh Bá Đạt","3/12/1999",0,"Bắc Ninh","0930153274","DatHBCE140320@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/DatHBCE140320.png"),
                new Student("CE140370","Trương Nhật Nam","2/9/1999",0,"Sóc Trăng","0323951723","NamTNCE140370@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/NamTNCE140370.png"),
                new Student("CE140381","Nguyễn Ngọc Anh Tú","17/1/1999",0,"Gia Lai","0371053258","TuNNACE140381@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/TuNNACE140381.png"),
                new Student("CE140386","Nguyễn Sơn Hào","28/10/1999",0,"An Giang","0838900787","HaoNSCE140386@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/HaoNSCE140386.png"),
                new Student("CE140415","Lê Minh Trí","7/11/1999",0,"Bà Rịa - Vũng Tàu","0785209064","TriLMCE140415@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/TriLMCE140415.png"),
                new Student("CE140454","Nguyễn Quốc Bảo","13/10/1999",0,"Bình Thuận","0962861147","BaoNQCE140454@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/BaoNQCE140454.png"),
                new Student("CE140466","Trần Nguyễn Đức Thịnh","21/4/1999",0,"Quảng Nam","0911326128","ThinhTNDCE140466@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/ThinhTNDCE140466.png"),
                new Student("CE140475","Võ Nhựt Hào","19/6/1999",0,"Ninh Thuận","0914179462","HaoVNCE140475@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/HaoVNCE140475.png"),
                new Student("CE140484","Trần Chấn Dương","1/12/1999",0,"Long An","0701961695","DuongTCCE140484@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/DuongTCCE140484.png"),
                new Student("CE140529","Châu Hữu Đang","19/6/1999",0,"Ninh Bình","0969998629","DangCHCE140529@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/DangCHCE140529.png"),
                new Student("CE140552","Hứa Thiên Ngân","14/9/1999",1,"Hà Giang","0772989116","NganHTCE140552@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/NganHTCE140552.png"),
                new Student("CE140623","Nguyễn Duy Quang Huy","12/11/1999",0,"Sóc Trăng","0584212579","HuyNDQCE140623@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/HuyNDQCE140623.png"),
                new Student("CE140628","Trần Hoàng Phúc","19/1/1999",0,"Hải Phòng","0986746725","PhucTHCE140628@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/PhucTHCE140628.png"),
                new Student("CE140632","Trần Hoàng Quí","1/10/1999",0,"Bến Tre","0376389649","QuiTHCE140632@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/QuiTHCE140632.png"),
                new Student("CE140664","Lê Gia Minh Phú","18/5/1999",0,"Bạc Liêu","0858994865","PhuLGMCE140664@fpt.edu.vn","http://localhost:8000/download/Students/SE1403/PhuLGMCE140664.png")
        );
        Clazz se1403class = clazzRepository.findFirstByCode("SE1403");
        se1403.forEach(student -> {
            student.setMajor(se);
            student.setClazz(se1403class);
            student.setStatus(status);
        });
        studentRepository.saveAll(se1403);



        List<Student> se1502 = Arrays.asList(
                new Student("CE130163","Trần Bá Trung Kiên","11/12/1999",0,"Trà Vinh","0880793335","kientbtce130163@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/kientbtce130163.png"),
                new Student("CE130431","Mã Anh Hoàng","11/7/1999",0,"Hưng Yên","0854150497","hoangmace130431@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/hoangmace130431.png"),
                new Student("CE140135","Hồ Minh Kỳ","9/10/1999",0,"Bắc Giang","0885660246","KyHMCE140135@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/KyHMCE140135.png"),
                new Student("CE140174","Nguyễn Thế Trung","1/7/1999",0,"Bà Rịa - Vũng Tàu","0902895240","TrungNTCE140174@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/TrungNTCE140174.png"),
                new Student("CE140212","Nguyễn Trọng Nghĩa","14/7/1999",0,"Vĩnh Phúc","0903696908","NghiaNTCE140212@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/NghiaNTCE140212.png"),
                new Student("CE140247","Thái Thanh Tân","22/1/1999",0,"Nam Định","0397067942","TanTTCE140247@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/TanTTCE140247.png"),
                new Student("CE140301","Huỳnh Ngọc Bảo Tâm","21/6/1999",0,"Tiền Giang","0934423405","TamHNBCE140301@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/TamHNBCE140301.png"),
                new Student("CE140347","Trương Khánh Tường","21/2/1999",0,"Lai Châu","0931057394","TuongTKCE140347@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/TuongTKCE140347.png"),
                new Student("CE140388","Phan Phú Duy Khang","8/6/1999",0,"Hà Nam","0869422826","KhangPPDCE140388@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/KhangPPDCE140388.png"),
                new Student("CE140485","Nguyễn Tấn An","20/9/1999",0,"Hà Nội","0331609296","AnNTCE140485@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/AnNTCE140485.png"),
                new Student("CE140524","Nguyễn Tấn Hiệp","23/1/1999",0,"Khánh Hòa","0936240673","HiepNTCE140524@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/HiepNTCE140524.png"),
                new Student("CE140649","Đổng Thành Luận","27/4/1999",0,"Sơn La","0907464468","LuanDTCE140649@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/LuanDTCE140649.png"),
                new Student("CE140668","Dương Hoàng Hiệp","16/9/1999",0,"Bình Dương","0379842766","HiepDHCE140668@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/HiepDHCE140668.png"),
                new Student("CE140669","Huỳnh Anh Kiệt","20/5/1999",0,"Thanh Hóa","0357092709","KietHACE140669@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/KietHACE140669.png"),
                new Student("CE150048","Huỳnh Bảo Long","4/7/1999",0,"Bắc Kạn","0794440182","LongHBCE150048@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/LongHBCE150048.png"),
                new Student("CE150059","Phan Phú Thịnh","2/4/1999",0,"Quảng Ninh","0707323604","ThinhPPCE150059@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/ThinhPPCE150059.png"),
                new Student("CE150061","Trần Đại Lĩnh Nam","13/12/1999",0,"Bắc Ninh","0965455870","NamTDLCE150061@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/NamTDLCE150061.png"),
                new Student("CE150093","Phạm Minh Nhí","17/12/1999",0,"Tiền Giang","0932173246","NhiPMCE150093@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/NhiPMCE150093.png"),
                new Student("CE150124","Nguyễn Quốc Trung Nhân","11/12/1999",0,"Thái Nguyên","0916490459","NhanNQTCE150124@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/NhanNQTCE150124.png"),
                new Student("CE150200","Võ Thanh Phong","13/11/1999",0,"Ninh Bình","0811489758","PhongVTCE150200@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/PhongVTCE150200.png"),
                new Student("CE150202","Trương Minh Quang","21/12/1999",0,"Nam Định","0937887176","QuangTMCE150202@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/QuangTMCE150202.png"),
                new Student("CE150217","Lê Đào Thiên Đức","21/10/1999",0,"Bà Rịa - Vũng Tàu","0974030201","DucLDTCE150217@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/DucLDTCE150217.png"),
                new Student("CE150240","Đặng Đỗ Hữu Bằng","3/8/1999",0,"Khánh Hòa","0588718916","BangDDHCE150240@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/BangDDHCE150240.png"),
                new Student("CE150352","Nguyễn Hồng Ái","26/7/1999",1,"Tiền Giang","0856781611","AiNHCE150352@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/AiNHCE150352.png"),
                new Student("CE150373","Lý Nguyễn Hoàng Phúc","16/2/1999",0,"Lâm Đồng","0975122080","PhucLNHCE150373@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/PhucLNHCE150373.png"),
                new Student("CE150394","Huỳnh Tấn Phúc","16/4/1999",0,"Lâm Đồng","0582200648","PhucHTCE150394@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/PhucHTCE150394.png"),
                new Student("CE150404","Đoàn Hiếu Nhân","20/1/1999",0,"Sơn La","0344596805","NhanDHCE150404@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/NhanDHCE150404.png"),
                new Student("CE150463","Nguyễn Tường Thành","15/5/1999",0,"Nam Định","0325221073","ThanhNTCE150463@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/ThanhNTCE150463.png"),
                new Student("CE150471","Lê Quốc Hùng","19/12/1999",0,"Nghệ An","0338592788","HungLQCE150471@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/HungLQCE150471.png"),
                new Student("CE150493","Phạm Đặng Lan Thịnh","1/6/1999",1,"Hưng Yên","0914695956","ThinhPDLCE150493@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/ThinhPDLCE150493.png"),
                new Student("CE150499","Trần Thị Hồng Mai","10/1/1999",1,"Đắk Lắk","0975439702","MaiTTHCE150499@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/MaiTTHCE150499.png"),
                new Student("CE150509","Lê Hoàng Quốc Bảo","21/12/1999",0,"Bình Dương","0983395173","BaoLHQCE150509@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/BaoLHQCE150509.png"),
                new Student("CE150519","Nguyễn Khánh Duy","27/2/1999",0,"Thanh Hóa","0332553679","DuyNKCE150519@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/DuyNKCE150519.png"),
                new Student("CE150521","Trần Văn Hảo","1/6/1999",0,"Đồng Tháp","0863293540","HaoTVCE150521@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/HaoTVCE150521.png"),
                new Student("CE150549","Trần Minh Nguyệt","25/8/1999",1,"Thanh Hóa","0923329300","NguyetTMCE150549@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/NguyetTMCE150549.png"),
                new Student("CE150579","Nguyễn Hoàng Phúc","28/4/1999",0,"Thừa Thiên Huế","0328983074","PhucNHCE150579@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/PhucNHCE150579.png"),
                new Student("CE150623","Trương Trung Tín","17/10/1999",0,"Bến Tre","0771323324","TinTTCE150623@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/TinTTCE150623.png"),
                new Student("CE150649","Lâm Bảo Duy","15/7/1999",0,"Phú Thọ","0375500266","DuyLBCE150649@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/DuyLBCE150649.png"),
                new Student("CE150659","Phan Công Hậu","25/9/1999",0,"Hoà Bình","0909013753","HauPCCE150659@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/HauPCCE150659.png"),
                new Student("CE150662","Nguyễn Đăng Khánh","18/6/1999",0,"Tây Ninh","0761940746","KhanhNDCE150662@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/KhanhNDCE150662.png"),
                new Student("CE150706","Trần Hòa Bình","20/7/1999",0,"Đắk Nông","0987600674","BinhTHCE150706@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/BinhTHCE150706.png"),
                new Student("CE150718","Lý Tuấn Đạt","7/6/1999",0,"Quảng Nam","0333387036","DatLTCE150718@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/DatLTCE150718.png"),
                new Student("CE150763","Phan Thanh Ngân","17/4/1999",1,"Nghệ An","0982988480","NganPTCE150763@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/NganPTCE150763.png"),
                new Student("CE150815","Trần Đắc Trí","25/12/1999",0,"Kiên Giang","0784433888","TriTDCE150815@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/TriTDCE150815.png"),
                new Student("CE150869","Hồ Hoàng Huynh","26/9/1999",0,"Vĩnh Phúc","0587476087","HuynhHHCE150869@fpt.edu.vn","http://localhost:8000/download/Students/SE1502/HuynhHHCE150869.png")
        );
        Clazz se1502class = clazzRepository.findFirstByCode("SE1502");
        se1502.forEach(student -> {
            student.setMajor(se);
            student.setClazz(se1502class);
            student.setStatus(status);
        });
        studentRepository.saveAll(se1502);


        List<Student> se15022 = Arrays.asList(
                new Student("CE130208","Lê Nguyễn Quốc Anh","19/10/1999",0,"Bến Tre","0390992347","anhlnqce130208@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/anhlnqce130208.png"),
                new Student("CE130421","Nguyễn Tuấn Bằng","3/11/1999",0,"Yên Bái","0786681826","bangntce130421@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/bangntce130421.png"),
                new Student("CE140040","Phạm Hữu Khang","15/8/1999",0,"Phú Yên","0961606274","KhangPHCE140040@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/KhangPHCE140040.png"),
                new Student("CE140212","Nguyễn Trọng Nghĩa","10/4/1999",0,"Quảng Ngãi","0708541968","NghiaNTCE140212@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/NghiaNTCE140212.png"),
                new Student("CE140242","Nguyễn Hứa Quốc Bảo","16/5/1999",0,"Lâm Đồng","0389200291","BaoNHQCE140242@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/BaoNHQCE140242.png"),
                new Student("CE140247","Thái Thanh Tân","18/1/1999",0,"Ninh Bình","0560376931","TanTTCE140247@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/TanTTCE140247.png"),
                new Student("CE140301","Huỳnh Ngọc Bảo Tâm","5/7/1999",0,"Nam Định","0946594034","TamHNBCE140301@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/TamHNBCE140301.png"),
                new Student("CE140388","Phan Phú Duy Khang","6/12/1999",0,"Hà Giang","0975480032","KhangPPDCE140388@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/KhangPPDCE140388.png"),
                new Student("CE140416","Huỳnh Quốc Di","23/5/1999",0,"Bến Tre","0355649321","DiHQCE140416@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/DiHQCE140416.png"),
                new Student("CE140426","Bùi Quách Thịnh","14/11/1999",0,"Bình Dương","0847237544","ThinhBQCE140426@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/ThinhBQCE140426.png"),
                new Student("CE140485","Nguyễn Tấn An","5/10/1999",0,"Đồng Tháp","0378509529","AnNTCE140485@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/AnNTCE140485.png"),
                new Student("CE140518","Lê Trung Hưng","28/8/1999",0,"Đồng Tháp","0889326735","HungLTCE140518@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/HungLTCE140518.png"),
                new Student("CE140522","Phạm Vũ Hoàng","25/3/1999",0,"Nam Định","0564260361","HoangPVCE140522@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/HoangPVCE140522.png"),
                new Student("CE140524","Nguyễn Tấn Hiệp","22/9/1999",0,"Bình Phước","0350446622","HiepNTCE140524@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/HiepNTCE140524.png"),
                new Student("CE140592","Lê Nguyễn Minh Hiền","7/10/1999",0,"Thanh Hóa","0339034578","HienLNMCE140592@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/HienLNMCE140592.png"),
                new Student("CE140649","Đổng Thành Luận","9/5/1999",0,"Hậu Giang","0923948341","LuanDTCE140649@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/LuanDTCE140649.png"),
                new Student("CE150048","Huỳnh Bảo Long","1/3/1999",0,"Hải Dương","0792571287","LongHBCE150048@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/LongHBCE150048.png"),
                new Student("CE150059","Phan Phú Thịnh","26/4/1999",0,"Bắc Ninh","0341056829","ThinhPPCE150059@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/ThinhPPCE150059.png"),
                new Student("CE150061","Trần Đại Lĩnh Nam","26/11/1999",0,"Lạng Sơn","0847064572","NamTDLCE150061@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/NamTDLCE150061.png"),
                new Student("CE150093","Phạm Minh Nhí","10/6/1999",0,"Vĩnh Phúc","0814475546","NhiPMCE150093@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/NhiPMCE150093.png"),
                new Student("CE150124","Nguyễn Quốc Trung Nhân","6/11/1999",0,"Quảng Bình","0359300085","NhanNQTCE150124@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/NhanNQTCE150124.png"),
                new Student("CE150200","Võ Thanh Phong","9/12/1999",0,"Ninh Bình","0902121839","PhongVTCE150200@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/PhongVTCE150200.png"),
                new Student("CE150202","Trương Minh Quang","20/6/1999",0,"Tiền Giang","0796104087","QuangTMCE150202@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/QuangTMCE150202.png"),
                new Student("CE150217","Lê Đào Thiên Đức","19/9/1999",0,"Ninh Bình","0881176588","DucLDTCE150217@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/DucLDTCE150217.png"),
                new Student("CE150240","Đặng Đỗ Hữu Bằng","18/12/1999",0,"Hưng Yên","0915423951","BangDDHCE150240@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/BangDDHCE150240.png"),
                new Student("CE150352","Nguyễn Hồng Ái","7/8/1999",1,"Hồ Chí Minh","0913125952","AiNHCE150352@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/AiNHCE150352.png"),
                new Student("CE150373","Lý Nguyễn Hoàng Phúc","24/2/1999",0,"Hà Tĩnh","0840615543","PhucLNHCE150373@fpt.edu.vn","http://localhost:8000/download/Students/SE1502.2/PhucLNHCE150373.png")
        );
        Clazz se15022class = clazzRepository.findFirstByCode("SE1502.2");
        se15022.forEach(student -> {
            student.setMajor(se);
            student.setClazz(se15022class);
            student.setStatus(status);
        });
        studentRepository.saveAll(se15022);



        List<Student> se1501 = Arrays.asList(
                new Student("CE130004","Nguyễn Lâm Sang","7/1/1999",0,"Hồ Chí Minh","0824767619","sangnlce130004@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/sangnlce130004.png"),
                new Student("CE130143","Võ Quang Huy","12/6/1999",0,"Hoà Bình","0789439561","huyvqce130143@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/huyvqce130143.png"),
                new Student("CE130173","Trần Phạm Thế Anh","25/11/1999",0,"Hà Tĩnh","0399996694","anhtptce130173@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/anhtptce130173.png"),
                new Student("CE130208","Lê Nguyễn Quốc Anh","14/9/1999",0,"Hải Phòng","0337036844","anhlnqce130208@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/anhlnqce130208.png"),
                new Student("CE130373","Nguyễn Hoàng Nam","26/3/1999",0,"Quảng Ninh","0865119325","namnhce130373@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/namnhce130373.png"),
                new Student("CE140040","Phạm Hữu Khang","20/2/1999",0,"Đà Nẵng","0380148011","KhangPHCE140040@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/KhangPHCE140040.png"),
                new Student("CE140135","Hồ Minh Kỳ","3/2/1999",0,"Bà Rịa - Vũng Tàu","0846403170","KyHMCE140135@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/KyHMCE140135.png"),
                new Student("CE140174","Nguyễn Thế Trung","8/9/1999",0,"Hải Dương","0327854770","TrungNTCE140174@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/TrungNTCE140174.png"),
                new Student("CE140278","Nguyễn Vĩnh Lạp","19/8/1999",0,"Gia Lai","0561659047","LapNVCE140278@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/LapNVCE140278.png"),
                new Student("CE140335","Lê Nhựt Toàn","25/2/1999",0,"Long An","0922955289","ToanLNCE140335@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/ToanLNCE140335.png"),
                new Student("CE140345","Võ Thành An","1/8/1999",1,"Quảng Nam","0931063711","AnVTCE140345@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/AnVTCE140345.png"),
                new Student("CE140426","Bùi Quách Thịnh","1/6/1999",0,"Quảng Ngãi","0928304078","ThinhBQCE140426@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/ThinhBQCE140426.png"),
                new Student("CE140447","Huỳnh Văn Danh","22/10/1999",0,"Hà Tĩnh","0913440260","DanhHVCE140447@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/DanhHVCE140447.png"),
                new Student("CE140466","Trần Nguyễn Đức Thịnh","1/2/1999",0,"Bình Thuận","0830642732","ThinhTNDCE140466@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/ThinhTNDCE140466.png"),
                new Student("CE140556","Huỳnh Hải Giang","28/9/1999",0,"Bình Thuận","0869943451","GiangHHCE140556@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/GiangHHCE140556.png"),
                new Student("CE140575","Nguyễn Thị Kim Loan","23/5/1999",1,"Quảng Ngãi","0821107831","LoanNTKCE140575@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/LoanNTKCE140575.png"),
                new Student("CE140643","Đỗ Thế An","26/6/1999",0,"An Giang","0820798488","AnDTCE140643@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/AnDTCE140643.png"),
                new Student("CE150034","Nguyễn Thanh Long","20/12/1999",0,"Hồ Chí Minh","0352403819","LongNTCE150034@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/LongNTCE150034.png"),
                new Student("CE150056","Lê Phạm Thành Danh","26/11/1999",0,"Quảng Ninh","0960962082","DanhLPTCE150056@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/DanhLPTCE150056.png"),
                new Student("CE150261","Phạm Tuấn Kiệt","19/3/1999",0,"Hậu Giang","0822776763","KietPTCE150261@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/KietPTCE150261.png"),
                new Student("CE150696","Nguyễn Lê Vỹ","16/8/1999",0,"Đồng Tháp","0867972183","VyNLCE150696@fpt.edu.vn","http://localhost:8000/download/Students/SE1501/VyNLCE150696.png")
        );
        Clazz se1501class = clazzRepository.findFirstByCode("SE1501");
        se1501.forEach(student -> {
            student.setMajor(se);
            student.setClazz(se1501class);
            student.setStatus(status);
        });
        studentRepository.saveAll(se1501);


        List<Student> se15012 = Arrays.asList(
                new Student("CE150004","Thượng Duy Khang","16/2/1999",0,"Quảng Ninh","0981092688","KhangTDCE150004@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/KhangTDCE150004.png"),
                new Student("CE150019","Tiêu Anh Thọ","2/8/1999",0,"Vĩnh Long","0769092228","ThoTACE150019@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/ThoTACE150019.png"),
                new Student("CE150022","Đoàn Ngọc Minh","6/7/1999",0,"Kiên Giang","0337994719","MinhDNCE150022@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/MinhDNCE150022.png"),
                new Student("CE150063","Tống Thanh Lâm","25/3/1999",0,"Bình Định","0396762773","LamTTCE150063@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/LamTTCE150063.png"),
                new Student("CE150075","Nguyễn Mạnh Tân","6/10/1999",0,"Long An","0981584921","TanNMCE150075@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/TanNMCE150075.png"),
                new Student("CE150099","Lê Thành Đạt","17/10/1999",0,"Khánh Hòa","0794672762","DatLTCE150099@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/DatLTCE150099.png"),
                new Student("CE150103","Nguyễn Minh Khôi","16/7/1999",0,"Vĩnh Phúc","0850974570","KhoiNMCE150103@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/KhoiNMCE150103.png"),
                new Student("CE150130","Huỳnh Quang Tiên","28/9/1999",0,"Tiền Giang","0378772975","TienHQCE150130@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/TienHQCE150130.png"),
                new Student("CE150141","Lê Hoàng Hải Long","12/1/1999",0,"Đồng Nai","0844635729","LongLHHCE150141@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/LongLHHCE150141.png"),
                new Student("CE150226","Nguyễn Nhật Hoàng","9/3/1999",0,"Thanh Hóa","0798921916","HoangNNCE150226@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/HoangNNCE150226.png"),
                new Student("CE150269","Trần Khánh Toàn","15/7/1999",0,"Hà Giang","0781888482","ToanTKCE150269@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/ToanTKCE150269.png"),
                new Student("CE150284","Nguyễn Hoàng Vĩ","25/8/1999",0,"Quảng Ngãi","0793381214","ViNHCE150284@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/ViNHCE150284.png"),
                new Student("CE150337","Ngô Nguyễn Hoàng Phúc","23/2/1999",0,"Bắc Ninh","0386250072","PhucNNHCE150337@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/PhucNNHCE150337.png"),
                new Student("CE150344","Lê Xuân Tú","22/5/1999",0,"Đắk Nông","0869358187","TuLXCE150344@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/TuLXCE150344.png"),
                new Student("CE150405","Phạm Bảo Lộc","19/1/1999",0,"Bình Định","0825026890","LocPBCE150405@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/LocPBCE150405.png"),
                new Student("CE150568","Nguyễn Phúc Thịnh","27/6/1999",0,"Bình Thuận","0939316457","ThinhNPCE150568@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/ThinhNPCE150568.png"),
                new Student("CE150569","Lê Trí Hào","15/9/1999",0,"Ninh Bình","0901421952","HaoLTCE150569@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/HaoLTCE150569.png"),
                new Student("CE150572","Nguyễn Văn Tiến","21/9/1999",0,"An Giang","0333318911","TienNVCE150572@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/TienNVCE150572.png"),
                new Student("CE150582","Hồ Khả Minh","7/5/1999",0,"An Giang","0963027047","MinhHKCE150582@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/MinhHKCE150582.png"),
                new Student("CE150599","Trần Thiện Phú","1/8/1999",0,"Hưng Yên","0834393441","PhuTTCE150599@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/PhuTTCE150599.png"),
                new Student("CE150661","Cao Hoàng Anh Uy","6/11/1999",0,"Nam Định","0929566310","UyCHACE150661@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/UyCHACE150661.png"),
                new Student("CE150879","Nguyễn Quốc Hưng","8/9/1999",0,"Đồng Nai","0940387156","HungNQCE150879@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/HungNQCE150879.png"),
                new Student("CE150881","Nguyễn Thanh Tâm","11/7/1999",0,"Thái Nguyên","0923490601","TamNTCE150881@fpt.edu.vn","http://localhost:8000/download/Students/SE1501.2/TamNTCE150881.png")
        );
        Clazz se15012class = clazzRepository.findFirstByCode("SE1501.2");
        se15012.forEach(student -> {
            student.setMajor(se);
            student.setClazz(se15012class);
            student.setStatus(status);
        });
        studentRepository.saveAll(se15012);


        List<Student> ia1401 = Arrays.asList(
                new Student("CE130386","Huỳnh Minh Thông","11/1/1999",0,"Nghệ An","0965047626","thonghmce130386@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/thonghmce130386.png"),
                new Student("CE140024","Nguyễn Phạm Đình Phúc","26/3/1999",0,"Kiên Giang","0922435460","PhucNPDCE140024@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/PhucNPDCE140024.png"),
                new Student("CE140133","Nguyễn Huỳnh Tuấn Khôi","22/10/1999",0,"Thanh Hóa","0377209295","KhoiNHTCE140133@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/KhoiNHTCE140133.png"),
                new Student("CE140137","Phạm Văn Trọng Nhân","14/8/1999",0,"Bắc Giang","0792932664","NhanPVTCE140137@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/NhanPVTCE140137.png"),
                new Student("CE140201","Nguyễn Bạch Phúc Khang","2/9/1999",0,"Bà Rịa - Vũng Tàu","0394530519","KhangNBPCE140201@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/KhangNBPCE140201.png"),
                new Student("CE140217","Lê Quốc Thái","24/5/1999",0,"An Giang","0862676181","ThaiLQCE140217@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/ThaiLQCE140217.png"),
                new Student("CE140237","Nguyễn Vương Khang Hy","14/11/1999",0,"Phú Yên","0779260906","HyNVKCE140237@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/HyNVKCE140237.png"),
                new Student("CE140347","Trương Khánh Tường","11/4/1999",0,"Hà Nam","0332879959","TuongTKCE140347@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/TuongTKCE140347.png"),
                new Student("CE140405","Lâm Võ Ngọc Dung","19/6/1999",1,"Đắk Nông","0379225893","DungLVNCE140405@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/DungLVNCE140405.png"),
                new Student("CE140418","Lê Hoàng Tiểu Phụng","7/3/1999",1,"Đồng Tháp","0374402187","PhungLHTCE140418@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/PhungLHTCE140418.png"),
                new Student("CE140419","Nguyễn Tấn Vủ","12/9/1999",0,"Kiên Giang","0386490394","VuNTCE140419@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/VuNTCE140419.png"),
                new Student("CE140422","Nguyễn Lý Phương Uyên","1/12/1999",1,"Quảng Bình","0769751233","UyenNLPCE140422@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/UyenNLPCE140422.png"),
                new Student("CE140451","Nguyễn An Khoa","4/4/1999",0,"Tây Ninh","0842652718","KhoaNACE140451@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/KhoaNACE140451.png"),
                new Student("CE140487","Phạm Hồng Sáng","3/7/1999",0,"Thanh Hóa","0321985906","SangPHCE140487@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/SangPHCE140487.png"),
                new Student("CE140502","Đào Anh Tuấn","28/10/1999",0,"Lạng Sơn","0869549218","TuanDACE140502@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/TuanDACE140502.png"),
                new Student("CE140557","Võ Tuệ Nam","7/10/1999",0,"Bắc Kạn","0335769459","NamVTCE140557@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/NamVTCE140557.png"),
                new Student("CE140589","Nguyễn Cao Thống","25/2/1999",0,"Kon Tum","0780704564","ThongNCCE140589@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/ThongNCCE140589.png"),
                new Student("CE140603","Nguyễn Minh Long","25/5/1999",0,"Nghệ An","0946302164","LongNMCE140603@fpt.edu.vn","http://localhost:8000/download/Students/IA1401/LongNMCE140603.png")
        );
        Major ia = majorRepository.findFirstByCode("IA");
        Clazz ia1401class = clazzRepository.findFirstByCode("IA1401");
        ia1401.forEach(student -> {
            student.setMajor(ia);
            student.setClazz(ia1401class);
            student.setStatus(status);
        });
        studentRepository.saveAll(ia1401);



        List<Student> si1401 = Arrays.asList(
                new Student("CE140007","Ngô Nguyễn Hồng Hải Long","14/2/1999",0,"Yên Bái","0845185377","LongNNHHCE140007@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/LongNNHHCE140007.png"),
                new Student("CE140019","Trịnh Đình Quang","5/7/1999",0,"Đắk Nông","0960694796","QuangTDCE140019@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/QuangTDCE140019.png"),
                new Student("CE140024","Nguyễn Phạm Đình Phúc","24/11/1999",0,"Long An","0381927628","PhucNPDCE140024@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/PhucNPDCE140024.png"),
                new Student("CE140069","Nguyễn Hào Phú","23/2/1999",0,"Quảng Trị","0389939466","PhuNHCE140069@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/PhuNHCE140069.png"),
                new Student("CE140137","Phạm Văn Trọng Nhân","25/7/1999",0,"Bắc Ninh","0885410517","NhanPVTCE140137@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/NhanPVTCE140137.png"),
                new Student("CE140196","Nguyễn Đức Tông","7/2/1999",0,"Yên Bái","0882758111","TongNDCE140196@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/TongNDCE140196.png"),
                new Student("CE140201","Nguyễn Bạch Phúc Khang","10/2/1999",0,"Bạc Liêu","0766603568","KhangNBPCE140201@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/KhangNBPCE140201.png"),
                new Student("CE140217","Lê Quốc Thái","21/11/1999",0,"Bình Phước","0769579133","ThaiLQCE140217@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/ThaiLQCE140217.png"),
                new Student("CE140237","Nguyễn Vương Khang Hy","11/1/1999",0,"Bến Tre","0845943676","HyNVKCE140237@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/HyNVKCE140237.png"),
                new Student("CE140273","Tất Huỳnh Anh Khôi","24/4/1999",0,"Bắc Ninh","0840697218","KhoiTHACE140273@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/KhoiTHACE140273.png"),
                new Student("CE140337","Võ Anh Nhiều","16/4/1999",0,"Hà Giang","0796140984","NhieuVACE140337@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/NhieuVACE140337.png"),
                new Student("CE140347","Trương Khánh Tường","7/1/1999",0,"Thái Bình","0924802104","TuongTKCE140347@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/TuongTKCE140347.png"),
                new Student("CE140350","Nguyễn Thúy Hiền","23/2/1999",1,"Bắc Giang","0761158754","HienNTCE140350@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/HienNTCE140350.png"),
                new Student("CE140417","Nguyễn Văn Khương","28/5/1999",0,"Đà Nẵng","0770429623","KhuongNVCE140417@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/KhuongNVCE140417.png"),
                new Student("CE140418","Lê Hoàng Tiểu Phụng","27/2/1999",1,"Đắk Lắk","0772714292","PhungLHTCE140418@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/PhungLHTCE140418.png"),
                new Student("CE140419","Nguyễn Tấn Vủ","20/6/1999",0,"Hải Dương","0883820636","VuNTCE140419@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/VuNTCE140419.png"),
                new Student("CE140422","Nguyễn Lý Phương Uyên","12/1/1999",1,"Thái Nguyên","0979015432","UyenNLPCE140422@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/UyenNLPCE140422.png"),
                new Student("CE140435","Nguyễn Thị Diễm Hương","5/2/1999",1,"Vĩnh Long","0946028699","HuongNTDCE140435@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/HuongNTDCE140435.png"),
                new Student("CE140467","Diệp Trí Thành","17/1/1999",0,"Khánh Hòa","0777249306","ThanhDTCE140467@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/ThanhDTCE140467.png"),
                new Student("CE140469","Lê Đức Hòa","10/10/1999",0,"Ninh Thuận","0336328490","HoaLDCE140469@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/HoaLDCE140469.png"),
                new Student("CE140487","Phạm Hồng Sáng","16/11/1999",0,"Điện Biên","0351795637","SangPHCE140487@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/SangPHCE140487.png"),
                new Student("CE140520","Trần Nguyễn Quốc Huy","21/8/1999",0,"Vĩnh Phúc","0589410550","HuyTNQCE140520@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/HuyTNQCE140520.png"),
                new Student("CE140548","Ngô Hoàn Tâm Huy","2/8/1999",0,"Hải Phòng","0917077497","HuyNHTCE140548@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/HuyNHTCE140548.png"),
                new Student("CE140557","Võ Tuệ Nam","20/4/1999",0,"Gia Lai","0979328879","NamVTCE140557@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/NamVTCE140557.png"),
                new Student("CE140561","Lê Chấn Nghiệp","15/8/1999",0,"Bắc Giang","0584085990","NghiepLCCE140561@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/NghiepLCCE140561.png"),
                new Student("CE140603","Nguyễn Minh Long","5/7/1999",0,"Lào Cai","0701785202","LongNMCE140603@fpt.edu.vn","http://localhost:8000/download/Students/SI1401/LongNMCE140603.png")
        );
        Clazz si1401class = clazzRepository.findFirstByCode("SI1401");
        si1401.forEach(student -> {
            student.setMajor(se);
            student.setClazz(si1401class);
            student.setStatus(status);
        });
        studentRepository.saveAll(si1401);

    }

    @Bean
    void ggAuthData() {
        GGAuth auth1 = new GGAuth("101799590051054652390");
        auth1.setTeacher(teacherRepository.findFirstByCode("KhanhVH"));
        auth1.setRole(roleRepository.getOne(2));
        ggAuthRepository.save(auth1);

        GGAuth auth2 = new GGAuth("102074441731477501002");
        auth2.setTeacher(teacherRepository.findFirstByCode("HuongLH3"));
        auth2.setRole(roleRepository.getOne(2));
        ggAuthRepository.save(auth2);

        GGAuth auth3 = new GGAuth("117548952962395589920");
        auth3.setTeacher(teacherRepository.findFirstByCode("DaQL"));
        auth3.setRole(roleRepository.getOne(2));
        ggAuthRepository.save(auth3);
    }

    @Bean
    void subjectSemesterNewScheduled() throws ParseException {
        String dateString = "25/5/2020";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = dateFormat.parse(dateString);

        List<SubjectSemesterDto> list = Arrays.asList(
                new SubjectSemesterDto(null,5,"ThaoVT",date,1,5,1,1,1,31,Arrays.asList(4,5,6,7,8,9,10,11,14,16,19,21,22,23,24,25,26,27,31), Collections.singletonList(17)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,2,1,1,4,Arrays.asList(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31),Arrays.asList(11,12,27)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,3,3,1,4,Arrays.asList(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31),Arrays.asList(13,28,29)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,4,2,1,4,Arrays.asList(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31),Arrays.asList(3,19,35)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,6,2,2,9,Arrays.asList(34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68),Arrays.asList(10,25,26)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,15,3,2,9,Arrays.asList(34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68),Arrays.asList(1,17,33)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,5,1,2,9,Arrays.asList(34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68),Arrays.asList(8,9,24)),
                new SubjectSemesterDto(null,3,"ThaoVT",date,1,5,12,2,3,9,Arrays.asList(69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96), Collections.singletonList(5)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,6,1,3,9,Arrays.asList(69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96),Arrays.asList(13,28,29)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,15,3,3,9,Arrays.asList(69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96),Arrays.asList(11,12,27)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,5,1,3,9,Arrays.asList(69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96),Arrays.asList(4,20,36)),
                new SubjectSemesterDto(null,3,"ThaoVT",date,1,5,12,2,4,10,Arrays.asList(97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126), Collections.singletonList(16)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,6,2,4,10,Arrays.asList(97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126),Arrays.asList(8,9,24)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,15,2,4,10,Arrays.asList(97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126),Arrays.asList(2,18,34)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,5,1,4,10,Arrays.asList(97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126),Arrays.asList(1,25,26)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,7,2,5,10,Arrays.asList(243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260),Arrays.asList(11,12,27)),
                new SubjectSemesterDto(null,5,"ThaoVT",date,1,5,8,2,5,10,Arrays.asList(243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260), Collections.singletonList(4)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,9,2,5,10,Arrays.asList(243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260),Arrays.asList(13,28,29)),
                new SubjectSemesterDto(null,6,"ThaoVT",date,1,5,16,2,5,10,Arrays.asList(243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260),Arrays.asList(20,36)),
                new SubjectSemesterDto(null,5,"ThaoVT",date,1,5,10,1,6,8,Arrays.asList(127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171),Arrays.asList(10)),
                new SubjectSemesterDto(null,5,"ThaoVT",date,1,5,11,3,6,30,Arrays.asList(127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171),Arrays.asList(9)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,14,1,6,8,Arrays.asList(127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171),Arrays.asList(0,16,32)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,14,1,7,8,Arrays.asList(172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198),Arrays.asList(2,18,34)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,13,3,8,8,Arrays.asList(199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219),Arrays.asList(3,19,35)),
                new SubjectSemesterDto(null,30,"ThaoVT",date,1,5,13,3,9,8,Arrays.asList(220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242),Arrays.asList(5,21,37)),
                new SubjectSemesterDto(null,3,"ThaoVT",date,1,5,12,2,10,10,Arrays.asList(261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286), Collections.singletonList(21))
        );

        list.forEach(ss -> subjectSemesterController.create(ss));
    }
}
