package com.akko.android.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClazzDto {
    private Integer id;
    private String code;
    private String description;

    private Integer statusId;
}
