package com.akko.android.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformationDto {
    private Integer id;
    private String code;
    private String fullName;
    private String birthday;
    private int gender;
    private String address;
    private String phone;
    private String email;
    private MultipartFile image;

    private Integer statusId;
}
