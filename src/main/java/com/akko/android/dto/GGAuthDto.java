package com.akko.android.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class GGAuthDto {
    private Integer id;
    private String GGAuthId;

    private Integer roleId;
    private Integer teacherId;
}
