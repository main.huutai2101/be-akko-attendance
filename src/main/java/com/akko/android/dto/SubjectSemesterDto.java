package com.akko.android.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubjectSemesterDto {
    private Integer id;
    private Integer numberOfSlots;
    private String advisor;
    private Date startDate;

    private Integer statusId;
    private Integer semesterId;
    private Integer subjectId;
    private Integer teacherId;
    private Integer clazzId;
    private Integer roomId;
    private List<Integer> studentIdList;
    private List<Integer> scheduled;
}
