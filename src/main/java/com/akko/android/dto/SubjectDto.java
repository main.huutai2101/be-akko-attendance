package com.akko.android.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubjectDto {
    private Integer id;
    private String code;
    private String name;
    private String description;

    private Integer statusId;
}
