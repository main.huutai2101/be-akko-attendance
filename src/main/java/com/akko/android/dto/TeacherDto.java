package com.akko.android.dto;

import com.akko.android.entity.GGAuth;
import com.akko.android.entity.Information;
import com.akko.android.entity.SubjectSemester;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherDto extends InformationDto {
    private String entryYear;
    private String experience;
    private MultipartFile coverImage;
}
