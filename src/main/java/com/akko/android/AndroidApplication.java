package com.akko.android;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AndroidApplication {
    public static void main(String[] args) {
        SpringApplication.run(AndroidApplication.class, args);
        System.out.println("Server running...");
    }
}
