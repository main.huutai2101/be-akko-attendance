package com.akko.android.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class DUFileProperties {
    private String uploadDir;
    private String uploadUri;
    private String uploadMulUri;
    private String downloadUri;

    public String getUploadDir() {
        return uploadDir;
    }
    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }

    public String getUploadUri() {
        return uploadUri;
    }

    public void setUploadUri(String uploadUri) {
        this.uploadUri = uploadUri;
    }

    public String getUploadMulUri() {
        return uploadMulUri;
    }

    public void setUploadMulUri(String uploadMulUri) {
        this.uploadMulUri = uploadMulUri;
    }

    public String getDownloadUri() {
        return downloadUri;
    }

    public void setDownloadUri(String downloadUri) {
        this.downloadUri = downloadUri;
    }
}