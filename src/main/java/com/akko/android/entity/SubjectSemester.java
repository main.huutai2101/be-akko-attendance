package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SubjectSemester {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer numberOfSlots;
    private String advisor;
    private Date startDate;

    @ManyToOne
    private Status status;

    @ManyToOne
    private Semester semester;

    @ManyToOne
    private Subject subject;

    @ManyToOne
    private Teacher teacher;

    @ManyToOne
    private Clazz clazz;

    @ManyToMany
    private List<Student> students;

    @OneToMany(mappedBy = "subjectSemester")
    @JsonIgnore
    private List<SubjectSlot> subjectSlots;

    public SubjectSemester() {
    }

    public SubjectSemester(Integer numberOfSlots, String advisor, Date startDate) {
        this.numberOfSlots = numberOfSlots;
        this.advisor = advisor;
        this.startDate = startDate;
    }

    public SubjectSemester(Integer id, Integer numberOfSlots, String advisor, Date startDate) {
        this.id = id;
        this.numberOfSlots = numberOfSlots;
        this.advisor = advisor;
        this.startDate = startDate;
    }
}
