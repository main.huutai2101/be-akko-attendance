package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Clazz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String description;

    @ManyToOne
    private Status status;

    @OneToMany(mappedBy = "clazz")
    @JsonIgnore
    private List<Student> students;

    @OneToMany(mappedBy = "clazz")
    @JsonIgnore
    private List<SubjectSemester> subjectSemesters;

    public Clazz() {
    }

    public Clazz(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Clazz(Integer id, String code, String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }
}
