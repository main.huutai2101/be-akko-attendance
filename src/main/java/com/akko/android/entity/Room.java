package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String floor;
    private Integer capacity;

    @ManyToOne
    private Status status;

    @OneToMany(mappedBy = "room")
    @JsonIgnore
    private List<SubjectSlot> subjectSlots;

    public Room() {
    }

    public Room(String code, String floor, Integer capacity) {
        this.code = code;
        this.floor = floor;
        this.capacity = capacity;
    }

    public Room(Integer id, String code, String floor, Integer capacity) {
        this.id = id;
        this.code = code;
        this.floor = floor;
        this.capacity = capacity;
    }
}
