package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Semester {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String description;

    @ManyToOne
    private Status status;

    @OneToMany(mappedBy = "semester")
    @JsonIgnore
    private List<SubjectSemester> subjectSemesters;

    public Semester() {
    }

    public Semester(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Semester(Integer id, String code, String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }
}
