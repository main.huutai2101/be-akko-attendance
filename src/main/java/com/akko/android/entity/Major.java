package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Major {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String description;

    @OneToMany(mappedBy = "major")
    @JsonIgnore
    private List<Student> students;

    @ManyToOne
    private Status status;


    public Major() {
    }

    public Major(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Major(Integer id, String code, String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }
}
