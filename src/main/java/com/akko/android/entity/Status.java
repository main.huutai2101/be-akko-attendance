package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String description;

    public Status() {
    }

    public Status(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Status(Integer id, String code, String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }
}
