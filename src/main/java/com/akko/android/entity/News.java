package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    @Column(columnDefinition = "TEXT")
    private String content;

    private String createdBy;
    private Date createdAt;

    public News() {
    }

    public News(Integer id, String title, String content, String createdBy) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.createdAt = new Date();
        this.createdBy = createdBy;
    }

    public News(String title, String content, String createdBy) {
        this.title = title;
        this.content = content;
        this.createdAt = new Date();
        this.createdBy = createdBy;
    }
}
