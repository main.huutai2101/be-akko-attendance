package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Student extends Information {
    @ManyToOne
    private Major major;

    @ManyToOne
    private Clazz clazz;

    @OneToMany(mappedBy = "student")
    @JsonIgnore
    private List<SubjectSession> subjectSessions;

    @ManyToMany(mappedBy = "students")
    @JsonIgnore
    private List<SubjectSemester> subjectSemesters;

    public Student(String code, String fullName, String birthday, int gender, String address, String phone, String email, String image) {
        super(code, fullName, birthday, gender, address, phone, email, image);
    }

    public Student(Integer id, String code, String fullName, String birthday, int gender, String address, String phone, String email, String image) {
        super(id, code, fullName, birthday, gender, address, phone, email, image);
    }

    public Student() {
    }
}
