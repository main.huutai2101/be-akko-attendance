package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Teacher extends Information{
    private String entryYear;
    private String experience;
    private String coverImage;

    @OneToOne(mappedBy = "teacher")
    @JsonIgnore
    private GGAuth ggAuth;

    @OneToMany(mappedBy = "teacher")
    @JsonIgnore
    private List<SubjectSemester> subjectSemesters;

    public Teacher() {
    }


    public Teacher(String code, String fullName, String birthday, int gender, String address, String phone, String email, String entryYear, String experience, String image, String coverImage) {
        super(code, fullName, birthday, gender, address, phone, email, image);
        this.entryYear = entryYear;
        this.experience = experience;
        this.coverImage = coverImage;
    }

    public Teacher(Integer id, String code, String fullName, String birthday, int gender, String address, String phone, String email, String entryYear, String experience, String image, String coverImage) {
        super(id, code, fullName, birthday, gender, address, phone, email, image);
        this.entryYear = entryYear;
        this.experience = experience;
        this.coverImage = coverImage;
    }
}
