package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Information {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String fullName;
    private String birthday;
    private int gender;
    private String address;
    private String phone;
    private String email;
    private String image;

    @ManyToOne
    private Status status;




    public Information() {
    }

    public Information(Integer id, String code, String fullName, String birthday, int gender, String address, String phone, String email, String image) {
        this.id = id;
        this.code = code;
        this.fullName = fullName;
        this.birthday = birthday;
        this.gender = gender;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.image = image;
    }

    public Information(String code, String fullName, String birthday, int gender, String address, String phone, String email, String image) {
        this.code = code;
        this.fullName = fullName;
        this.birthday = birthday;
        this.gender = gender;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.image = image;
    }
}
