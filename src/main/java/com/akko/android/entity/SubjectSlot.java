package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SubjectSlot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer slotNumber;
    private Date date;

    @ManyToOne
    private Status status;

    @ManyToOne
    private SubjectSemester subjectSemester;

    @ManyToOne
    private Room room;

    @OneToMany(mappedBy = "slot")
    @JsonIgnore
    private List<SubjectSession> subjectSessions;

    public SubjectSlot() {
    }

    public SubjectSlot(Integer slotNumber, Date date) {
        this.slotNumber = slotNumber;
        this.date = date;
    }

    public SubjectSlot(Integer id, Integer slotNumber, Date date) {
        this.id = id;
        this.slotNumber = slotNumber;
        this.date = date;
    }
}
