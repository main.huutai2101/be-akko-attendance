package com.akko.android.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GGAuth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String GGAuthId;

    @ManyToOne
    private Role role;

    @OneToOne
    private Teacher teacher;

    public GGAuth() {
    }

    public GGAuth(String GGAuthId) {
        this.GGAuthId = GGAuthId;
    }

    public GGAuth(Integer id, String GGAuthId) {
        this.id = id;
        this.GGAuthId = GGAuthId;
    }
}
